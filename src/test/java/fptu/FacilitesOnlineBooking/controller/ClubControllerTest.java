package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.*;
import fptu.FacilitesOnlineBooking.service.IAccountService;
import fptu.FacilitesOnlineBooking.service.IClubService;
import fptu.FacilitesOnlineBooking.service.impl.AccountService;
import fptu.FacilitesOnlineBooking.service.impl.ClubService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class ClubControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClubService mockClubService;
    @MockBean
    private AccountService mockAccountService;
    public String login() throws Exception {
        String email = "anhnvhe130715@fpt.edu.vn";
        String fullName = "Ngo Viet Anh";
        String password = "";
        String body = "{\"email\":\"" + email + "\", \"fullName\":\""
                + fullName + "\", \"passWord\":\""+ password + "\"}";
        MvcResult result = this.mockMvc.perform(post("/authenticate/login").content(body).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String response = result.getResponse().getContentAsString();
        String trim = response.substring(13);
        String token ="";
        if(trim.contains("\",\"userName\":\"anhnvhe130715@fpt.edu.vn\",\"fullName\":\"Ngo Viet Anh\",\"role\":\"ROLE_ADMIN\",\"id\":7,\"requestTypeStatus\":true,\"clubmember\":false,\"eventmember\":true}")) {
            token = trim.replace("\",\"userName\":\"anhnvhe130715@fpt.edu.vn\",\"fullName\":\"Ngo Viet Anh\",\"role\":\"ROLE_ADMIN\",\"id\":7,\"requestTypeStatus\":true,\"clubmember\":false,\"eventmember\":true}", "");
        }else{
            token = trim.replace("\",\"userName\":\"anhnvhe130715@fpt.edu.vn\",\"fullName\":\"Ngo Viet Anh\",\"role\":\"ROLE_ADMIN\",\"id\":7,\"requestTypeStatus\":true,\"eventmember\":true,\"clubmember\":false}", "");
        }
        return token;
    }
    @Test
    void testCreateClub() throws Exception {
        // Setup
        // Configure IClubService.getClubByName(...).
        final List<ClubDetail> clubDetails = List.of(new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getClubByName("clubName")).thenReturn(clubDetails);

        when(mockClubService.createClub(any(ClubDetail.class))).thenReturn(0);

        // Configure IAccountService.getAccountById(...).
        final Account account = new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(new ClubDetail(0L, "clubName", false, Set.of())), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0);
        when(mockAccountService.getAccountById(0L)).thenReturn(account);

        when(mockAccountService.updateAccount(any(Account.class))).thenReturn(0L);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/icpdp/addClubDetail")
                .header("Authorization", "Bearer " + login())
                .content("content").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void testCreateClub_IClubServiceGetClubByNameReturnsNoItems() throws Exception {
        // Setup
        when(mockClubService.getClubByName("clubName")).thenReturn(Collections.emptyList());
        when(mockClubService.createClub(any(ClubDetail.class))).thenReturn(0);

        // Configure IAccountService.getAccountById(...).
        final Account account = new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(new ClubDetail(0L, "clubName", false, Set.of())), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0);
        when(mockAccountService.getAccountById(0L)).thenReturn(account);

        when(mockAccountService.updateAccount(any(Account.class))).thenReturn(0L);
        String token = login();
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/icpdp/addClubDetail")
                .header("Authorization", "Bearer " + token)
                .content("content").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void testFindAll() throws Exception {
        // Setup
        // Configure IClubService.getAllClub(...).
        final List<ClubDetail> clubDetails = List.of(new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getAllClub()).thenReturn(clubDetails);
        String token = login();
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/ClubDetail")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testFindAll_IClubServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockClubService.getAllClub()).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/ClubDetail")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testGetFacilityDetail() throws Exception {
        // Setup
        // Configure IClubService.getClubDetail(...).
        final Optional<ClubDetail> clubDetail = Optional.of(new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getClubDetail(0L)).thenReturn(clubDetail);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/getClubDetail/{id}", 0)
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }


    @Test
    void testGetFacilityByName() throws Exception {
        // Setup
        // Configure IClubService.getClubByName(...).
        final List<ClubDetail> clubDetails = List.of(new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getClubByName("clubName")).thenReturn(clubDetails);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/getClubByName/{clubName}", "clubName")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testGetFacilityByName_IClubServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockClubService.getClubByName("clubName")).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/getClubByName/{clubName}", "clubName")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testUpdateClub() throws Exception {
        // Setup
        // Configure IClubService.getClubDetail(...).
        /*final Optional<ClubDetail> clubDetail = Optional.of(new ClubDetail(21L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getClubDetail(21L)).thenReturn(clubDetail);

        // Configure IClubService.updateClub(...).
        final ClubDetail clubDetail1 = new ClubDetail(21L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0)));
        when(mockClubService.updateClub(any(ClubDetail.class))).thenReturn(clubDetail1);

        // Configure IAccountService.getAccountById(...).
        final Account account = new Account(21L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(new ClubDetail(0L, "clubName", false, Set.of())), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0);
        when(mockAccountService.getAccountById(21L)).thenReturn(account);

        when(mockAccountService.updateAccount(any(Account.class))).thenReturn(0L);
*/
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(put("/api/icpdp/updateClub/21")
                .header("Authorization", "Bearer " + login())
                .content("content").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
       /* verify(mockClubService).updateClub(any(ClubDetail.class));
        verify(mockAccountService).updateAccount(any(Account.class));*/
    }

    @Test
    void testUpdateClub_IClubServiceGetClubDetailReturnsAbsent() throws Exception {
        // Setup
        when(mockClubService.getClubDetail(0L)).thenReturn(Optional.empty());

        // Configure IClubService.updateClub(...).
        final ClubDetail clubDetail = new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0)));
        when(mockClubService.updateClub(any(ClubDetail.class))).thenReturn(clubDetail);

        // Configure IAccountService.getAccountById(...).
        final Account account = new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(new ClubDetail(0L, "clubName", false, Set.of())), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0);
        when(mockAccountService.getAccountById(0L)).thenReturn(account);

        when(mockAccountService.updateAccount(any(Account.class))).thenReturn(0L);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(put("/api/icpdp/updateClub/{id}", 0)
                .header("Authorization", "Bearer " + login())
                .content("content").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }


    @Test
    void testExportToCSV() throws Exception {
        // Setup
        // Configure IClubService.getAllClub(...).
        final List<ClubDetail> clubDetails = List.of(new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getAllClub()).thenReturn(clubDetails);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/exportClub")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testExportToCSV_IClubServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockClubService.getAllClub()).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/exportClub")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testExportToCSVTemplate() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/exportClubTemplate")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testExportToCSVTemplate_ThrowsIOException() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/exportClubTemplate")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testUploadFile() throws Exception {
        // Setup
        // Configure IClubService.save(...).
        final FileImportError fileImportError = new FileImportError(List.of("value"), 0, 0);
        when(mockClubService.save(any(MultipartFile.class))).thenReturn(fileImportError);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/icpdp/upload")
                .file(new MockMultipartFile("file", "originalFilename", MediaType.APPLICATION_JSON_VALUE, "content".getBytes()))
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }


    @Test
    void testFindAllDB() throws Exception {
        // Setup
        // Configure IClubService.getAllClubDB(...).
        final List<ClubDetail> clubDetails = List.of(new ClubDetail(0L, "clubName", false, Set.of(new Account(0L, "email", "fullName", "role", List.of(new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false)), Set.of(), Set.of(new EventDetail(0L, "eventName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), false, Set.of())), List.of(new RequestLog(0L, null, new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), null, "requestDetailStatus")), false), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(new RequestDetail(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "timeUsing", false, false, new Facility(0L, "facilityName", false, false, false, List.of()), new Request(0L, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeRequest", "note", "status", false, null, List.of(new RequestLog(0L, null, null, new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), "typeChange", false)), List.of(), false), "requestDetailStatus")), "passWord", false, 0))));
        when(mockClubService.getAllClubDB()).thenReturn(clubDetails);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/getAllClubDB")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void testFindAllDB_IClubServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockClubService.getAllClubDB()).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/icpdp/getAllClubDB")
                .header("Authorization", "Bearer " + login())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}
