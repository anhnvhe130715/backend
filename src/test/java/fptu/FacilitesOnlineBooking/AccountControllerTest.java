package fptu.FacilitesOnlineBooking;

import fptu.FacilitesOnlineBooking.controller.AccountController;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.service.impl.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService service;
    public String login() throws Exception {
        String email = "anhnvhe130715@fpt.edu.vn";
        String fullName = "Ngo Viet Anh";
        String password = "";
        String body = "{\"email\":\"" + email + "\", \"fullName\":\""
                + fullName + "\", \"passWord\":\""+ password + "\"}";
        MvcResult result = this.mockMvc.perform(post("/authenticate/login").content(body).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String response = result.getResponse().getContentAsString();
        String trim = response.substring(13);
        String token ="";
        if(trim.contains("\",\"userName\":\"anhnvhe130715@fpt.edu.vn\",\"fullName\":\"Ngo Viet Anh\",\"role\":\"ROLE_ADMIN\",\"id\":7,\"requestTypeStatus\":true,\"clubmember\":false,\"eventmember\":true}")) {
            token = trim.replace("\",\"userName\":\"anhnvhe130715@fpt.edu.vn\",\"fullName\":\"Ngo Viet Anh\",\"role\":\"ROLE_ADMIN\",\"id\":7,\"requestTypeStatus\":true,\"clubmember\":false,\"eventmember\":true}", "");
        }else{
            token = trim.replace("\",\"userName\":\"anhnvhe130715@fpt.edu.vn\",\"fullName\":\"Ngo Viet Anh\",\"role\":\"ROLE_ADMIN\",\"id\":7,\"requestTypeStatus\":true,\"eventmember\":true,\"clubmember\":false}", "");
        }
        return token;
    }
    @Test
    public void getAllAccountInDBWithoutLogin() throws Exception {
        List<Account> mockListAccount = new ArrayList<>();
        when(service.getListAccount()).thenReturn(mockListAccount);
        this.mockMvc.perform(get("/api/icpdp/ListAllAccount")).andExpect(status().isUnauthorized()).andReturn();
    }
    @Test
    public void getAllAccountInDBSuccess() throws Exception {
        this.mockMvc.perform(get("/api/icpdp/ListAllAccount").header("Authorization", "Bearer " + login())).andExpect(status().isOk()).andReturn();
    }
    @Test
    public void ListAllAccountInByEmailDBSuccess() throws Exception {
        this.mockMvc.perform(get("/api/icpdp/ListAllAccount/anhnvhe130715@fpt.edu.vn").header("Authorization", "Bearer " + login())).andExpect(status().isOk()).andReturn();
    }
    @Test
    public void ListAllAccountInByEmailListAllAccountInByEmailDBWithoutLogin() throws Exception {
        List<Account> mockListAccount = new ArrayList<>();
        when(service.getListAccount()).thenReturn(mockListAccount);
        this.mockMvc.perform(get("/api/icpdp/ListAllAccount/anhnvhe130715@fpt.edu.vn")).andExpect(status().isUnauthorized()).andReturn();
    }

}