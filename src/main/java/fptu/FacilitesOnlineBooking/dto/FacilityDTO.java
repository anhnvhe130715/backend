package fptu.FacilitesOnlineBooking.dto;

import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FacilityDTO {
    private Long id;
    private String facilityName;
    private boolean isBuilding;
    private boolean isAvailable;
    private boolean deleteStatus;
    private List<Facility> room;
    private Facility buildings;
    private Long capacity;
    private boolean isNotForPersonal;
    private boolean forClubEvent;
}
