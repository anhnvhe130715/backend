package fptu.FacilitesOnlineBooking.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class RequestDetailDTO implements Comparable<RequestDetailDTO>{
    private Long id;
    private Date useDate;
    private String timeUsing;
    private boolean deleteStatus;
    private Facility facility;
    private Request request;
    private String requestDetailStatus;
    public RequestDetailDTO(Date useDate) {
        this.useDate = useDate;
    }

    @Override
    public int compareTo(RequestDetailDTO o) {
        return this.getUseDate().compareTo(o.getUseDate());
    }
}
