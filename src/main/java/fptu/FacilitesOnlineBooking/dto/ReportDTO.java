package fptu.FacilitesOnlineBooking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="report")
public class ReportDTO {
    private Long id;
    private String subject;
    private String message;
    private String email;
}
