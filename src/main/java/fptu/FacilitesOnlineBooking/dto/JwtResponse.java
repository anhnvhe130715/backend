package fptu.FacilitesOnlineBooking.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private String jwtToken;
    private String userName;
    private String fullName;
    private String role;
    private Long id;
    private boolean requestTypeStatus ;
    private boolean isClubmember;
    private boolean isEventmember;
}
