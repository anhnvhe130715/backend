package fptu.FacilitesOnlineBooking.dto;

import fptu.FacilitesOnlineBooking.model.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {
    private Long id;
    private String email;
    private String fullName;
    private String role;
    private List<Request> requests;
    private List<RequestLog> requestLogs;
    private List<RequestDetail> requestDetails;
    private boolean clubStatus;
    private boolean eventStatus;
    private boolean requestTypeStatus;
}
