package fptu.FacilitesOnlineBooking.dto;

import fptu.FacilitesOnlineBooking.model.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EventDTO {
    private Long id;
    private String eventName;
    private Date fromDate;
    private Date toDate;
    private boolean deleteStatus;
    private List<Account> accounts;
}
