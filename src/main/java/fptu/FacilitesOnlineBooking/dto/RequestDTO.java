package fptu.FacilitesOnlineBooking.dto;

import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.model.RequestLog;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class RequestDTO implements Comparable<RequestDTO>{
    private Long id;
    private Date dateCreated;
    private Date dateModified;
    private String typeRequest;
    private String note;
    private String status;
    private Account account;
    private List<RequestLog> requestLogs;
    private List<RequestDetail> requestDetails;
    private boolean isBookedByClub;

    @Override
    public int compareTo(RequestDTO o) {
        return this.getDateCreated().compareTo(o.getDateCreated());
    }
}
