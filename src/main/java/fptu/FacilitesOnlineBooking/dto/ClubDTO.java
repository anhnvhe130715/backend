package fptu.FacilitesOnlineBooking.dto;

import fptu.FacilitesOnlineBooking.model.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClubDTO {
    private Long id;
    private String clubName;
    private boolean deleteStatus;
    private List<Account> accounts;
}
