package fptu.FacilitesOnlineBooking.dto;

import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.Request;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestLogDTO {
    private Long id;
    private Account account;
    private RequestDTO request;
    private Date dateLog;
    private  String typeChange;
    private boolean deleteStatus;
}
