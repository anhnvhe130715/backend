Chào em,
Yêu cầu sử dụng phòng của em đã được chấp thuận. Em vui lòng kiểm tra trạng thái trên trang web https://facilities-booking-e6c56.web.app/
Thông báo kiểm tra thông báo xác nhận sử dụng phòng sẽ được gửi trước ngày sử dụng phòng 1 ngày. Em nhớ kiểm tra và xác nhận để được sử dụng phòng.
Thân gửi,
Phòng IC-PDP