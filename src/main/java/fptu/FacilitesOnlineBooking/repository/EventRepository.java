package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<EventDetail,Long> {
    @Query(value="select * from event_detail e where e.event_name like %:eventName% and e.delete_status = false", nativeQuery=true)
    List<EventDetail> findByEventName(@Param("eventName") String eventName);
    List<EventDetail> findAllByDeleteStatus(boolean deleteStatus);
    List<EventDetail> findAllByToDateBefore(Date toDate);
}
