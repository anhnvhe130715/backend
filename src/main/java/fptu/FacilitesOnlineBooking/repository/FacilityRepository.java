package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface FacilityRepository  extends JpaRepository<Facility, Long> {
    @Query(value="select * from facility f where f.facility_name like %:facilityName% and f.delete_status = false", nativeQuery=true)
    List<Facility> findByFacilityName(@Param("facilityName") String facilityName);
    List<Facility> findByisBuilding(boolean isBuilding);
    List<Facility> findByBuildings(Facility building);
}





