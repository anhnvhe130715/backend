package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@Repository
public interface RequestRepository extends JpaRepository<Request,Long> {
    List<Request> findByAccountId(Long Id);
    List<Request> findByStatus(String status);
    List<Request> findByRequestDetails(RequestDetail requestDetail);
    List<Request> findAllByRequestDetailsIn(List<RequestDetail> requestDetails);
    List<Request> findByIsBookedByClub(boolean isBookByClub);
}
