package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.Email;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EmailRepository extends JpaRepository<Email,Long> {
    List<Email>findAllBySubject(String subject);
}
