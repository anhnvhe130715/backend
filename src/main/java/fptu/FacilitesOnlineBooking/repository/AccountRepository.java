package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByEmail(String email);
    List<Account> findByClubMembers(ClubDetail clubDetail);
    @Query(value="select * from user_account ua where ua.email like %:email%", nativeQuery=true)
    List<Account>searchByEmail(@Param("email") String email);
    List<Account> findAllByRequestDetailsIn(List<RequestDetail> details);
}
