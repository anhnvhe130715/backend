package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ClubRepository extends JpaRepository<ClubDetail, Long> {
    @Query(value="select * from club_detail c where c.club_Name like %:clubName% and c.delete_status = false", nativeQuery=true)
    List<ClubDetail> findByclubName(@Param("clubName") String clubName);
    @Query(value="DELETE FROM club_member WHERE account_id = :acc_id and club_id = :club_id", nativeQuery=true)
    List<ClubDetail> deleteClubMember(@Param("acc_id") Long accId,@Param("club_id") Long clubId);
}
