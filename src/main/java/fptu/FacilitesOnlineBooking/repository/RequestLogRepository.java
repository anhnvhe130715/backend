package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.model.RequestLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RequestLogRepository extends JpaRepository<RequestLog, Long> {
    @Query(value="select * from request_log rl where rl.type_change like %:typeChange% and rl.delete_status = false", nativeQuery=true)
    List<RequestLog> findByTypeChange(@Param("typeChange") String typeChange);
    List<RequestLog> findByRequest(Request request);
    @Query(value="select * from request_log rl where DATE_FORMAT(rl.date_log, '%Y-%m-%d') = :dateLog", nativeQuery=true)
    List<RequestLog> findByDateLog(@Param("dateLog") String dateLog);
}
