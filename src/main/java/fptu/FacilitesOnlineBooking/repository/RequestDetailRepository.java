package fptu.FacilitesOnlineBooking.repository;

import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RequestDetailRepository extends JpaRepository<RequestDetail,Long> {
    //@Query(value="select * from request_detail rd where rd.facility_id = :facilityId", nativeQuery=true)
    List<RequestDetail> findByFacility(Facility facility);
    List<RequestDetail> findByRequest(Request request);
    @Query(value="select * from request_detail rd where DATE_FORMAT(rd.use_date, '%Y-%m-%d') = :date", nativeQuery=true)
    List<RequestDetail> findByUseDate(@Param("date") String date);
    @Query(value="select * from request_detail rd\n" +
            "inner join request r on r.id = rd.request_id\n" +
            " where DATE (rd.use_date) <= DATE(:date) and r.status = :status", nativeQuery=true)
    List<RequestDetail> findAllByUseDateBeforeAndStatus(@Param("date") Date date, @Param("status") String status);
}
