package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.dto.ReportDTO;
import fptu.FacilitesOnlineBooking.service.IReportService;
import lombok.AllArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ReportService implements IReportService {
    private final JavaMailSender sender;
    @Override
    public void sendReport(ReportDTO reportDTO) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom(reportDTO.getEmail());
        mail.setTo("sonnghe130316@fpt.edu.vn");
        mail.setSubject(reportDTO.getSubject() + reportDTO.getEmail());
        mail.setText(reportDTO.getMessage());
        sender.send(mail);
    }
}
