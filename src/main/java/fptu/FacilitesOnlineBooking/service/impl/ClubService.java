package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.common.ExcelHelper;
import fptu.FacilitesOnlineBooking.dto.ClubDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.repository.AccountRepository;
import fptu.FacilitesOnlineBooking.repository.ClubRepository;
import fptu.FacilitesOnlineBooking.service.IClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class ClubService implements IClubService {
    @Autowired
    ClubRepository clb;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    ExcelHelper excel;
    @Override
    public int createClub(ClubDetail clubDetail) {
        clb.save(clubDetail);
        return 1;
    }

    @Override
    public Optional<ClubDetail> getClubDetail(Long id) {
        Optional<ClubDetail> clubDetail = clb.findById(id);
        if (clubDetail.isPresent() && clubDetail.get().isDeleteStatus() == false) {
            return clubDetail;
        }
        return null;
    }

    @Override
    public List<ClubDetail> getAllClub() {
        List<ClubDetail> clubDetails = clb.findAll();
        List<ClubDetail> result = new ArrayList<>();
        for (int i = 0; i < clubDetails.size(); i++) {
            if (clubDetails.get(i).isDeleteStatus() == false) {
                result.add(clubDetails.get(i));
            }
        }
        return result;
    }

    @Override
    public List<ClubDetail> getClubByName(String clubName) {
        List<ClubDetail> clubDetails = clb.findByclubName(clubName);
        List<ClubDetail> result = new ArrayList<>();
        if (!clubDetails.isEmpty()) {
            for (int i = 0; i < clubDetails.size(); i++) {
                result.add(clubDetails.get(i));
            }
            return result;
        }
        return null;
    }

    @Override
    public ClubDetail updateClub(ClubDetail clubDetail) {
        if (clubDetail.getClubName() != null && clubDetail.isDeleteStatus() == false) {
            ClubDetail updatedClub = clb.save(clubDetail);
            return new ClubDetail(updatedClub.getId(), updatedClub.getClubName(), updatedClub.isDeleteStatus(), updatedClub.getAccounts());
        } else {
            return null;
        }
    }

    @Override
    public Long deleteClub(long id) {
        try {
            Optional<ClubDetail> clubDetail = clb.findById(id);
            if (clubDetail.isPresent() && clubDetail.get().isDeleteStatus() == false) {
                clubDetail.get().setDeleteStatus(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ClubDTO mapDTO(ClubDetail clubDetail) {
        ClubDTO clubDTO = new ClubDTO();
        clubDTO.setId(clubDetail.getId());
        clubDTO.setClubName(clubDetail.getClubName());
        clubDTO.setDeleteStatus(clubDetail.isDeleteStatus());
        clubDTO.setAccounts(List.copyOf(clubDetail.getAccounts()));
        return clubDTO;
    }

    @Override
    public ClubDetail mapEntity(ClubDTO clubDTO) {
        ClubDetail clubDetail = new ClubDetail();
        clubDetail.setId(clubDTO.getId());
        clubDetail.setClubName(clubDTO.getClubName());
        clubDetail.setDeleteStatus(clubDTO.isDeleteStatus());
        clubDetail.setAccounts(Set.copyOf(clubDTO.getAccounts()));
        return clubDetail;
    }

    @Override
    public FileImportError save(MultipartFile file) {
        FileImportError error = new FileImportError();
            try {
                List<ClubDetail> clubDetails = excel.csvToClub(file.getInputStream());
                int countFile = 0;
                int countDB  = 0;
                List<String> errorLine = new ArrayList<>();
                for(int i = 0; i<clubDetails.size();i++)
                {
                    for(int j = i+1;j<clubDetails.size();j++)
                    {
                        if(clubDetails.get(i).getClubName().equalsIgnoreCase(clubDetails.get(j).getClubName()))
                        {
                            errorLine.add(clubDetails.get(i).getClubName()+" //in File");
                            countFile++;
                            //throw new RuntimeException("fail to store csv data: Duplicate data in file" + clubDetails.get(i).getClubName());
                        }
                    }
                    if(!clb.findByclubName(clubDetails.get(i).getClubName()).isEmpty()) {
                        errorLine.add(clubDetails.get(i).getClubName()+ " //in Database");
                        countDB++;
                    }
                }
                if(countDB != 0 || countFile != 0)
                {
                    error.setErrorLocation(errorLine);
                    error.setErrorInDBNumber(countDB);
                    error.setErrorInFileNumber(countFile);
                }
                else {
                    clb.saveAll(clubDetails);
                }
            } catch (IOException e) {
                throw new RuntimeException("fail to store csv data: " + e.getMessage());
            }
        return error;
    }
    @Override
    public void deleteClubDB(ClubDetail clubDetail) {
    clb.delete(clubDetail);
    }

    @Override
    public List<ClubDetail> getAllClubDB() {
        List<ClubDetail> result = clb.findAll();
        return result;
    }

    @Override
    public ClubDetail getClubDetailDB(Long id) {
        ClubDetail clubDetail = clb.findById(id).get();
        return  clubDetail;
    }

}
