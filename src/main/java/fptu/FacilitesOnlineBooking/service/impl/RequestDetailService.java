package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.dto.RequestDetailDTO;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.repository.AccountRepository;
import fptu.FacilitesOnlineBooking.repository.RequestDetailRepository;
import fptu.FacilitesOnlineBooking.repository.RequestRepository;
import fptu.FacilitesOnlineBooking.service.IRequestDetailService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class RequestDetailService implements IRequestDetailService {
    RequestDetailRepository repos;
    RequestRepository requestRepos;
    AccountRepository accountRepository;
    private final JavaMailSender sender;

    @Override
    public Long createRequest(RequestDetail requestDetail) {
        repos.save(requestDetail);
        return requestDetail.getId();
    }

    @Override
    public RequestDetail getRequestDetail(Long id) {

        return repos.findById(id).get();
    }

    @Override
    public Long deleteRequestDetail(RequestDetail requestDetail) { ;
        requestDetail.setDeleteStatus(true);
        repos.save(requestDetail);
        return requestDetail.getId();
    }

    @Override
    public Optional<RequestDetail> getRequestByRequest(Request request) {

        return Optional.empty();
    }

    @Override
    public List<RequestDetail> getRequestDetailByFacility(Facility facility) {
        List<RequestDetail> lstRequestDetail = repos.findByFacility(facility);
        List<RequestDetail> result = new ArrayList<>();
        if(!lstRequestDetail.isEmpty())
        {
            for(int i = 0;i <lstRequestDetail.size();i++)
            {
                Request request = requestRepos.findByRequestDetails(lstRequestDetail.get(i)).get(0);
                if(lstRequestDetail.get(i).isDeleteStatus() == false && !request.getStatus().equals("REJECTED"))
                {
                    result.add(lstRequestDetail.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public List<RequestDetail> getRequestDetailByRequest(Request request) {
        List<RequestDetail> lstRequestDetail = repos.findByRequest(request);
        List<RequestDetail> result = new ArrayList<>();
        if(!lstRequestDetail.isEmpty())
        {
            for(int i = 0;i <lstRequestDetail.size();i++)
            {
                if(lstRequestDetail.get(i).isDeleteStatus() == false)
                {
                    result.add(lstRequestDetail.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public Long updateRequestDetail(RequestDetail requestDetail, String status) {
        requestDetail.setRequestDetailStatus(status);
        repos.save(requestDetail);
        return requestDetail.getId();
    }

    @Override
    public List<RequestDetail> getRequestDetailByDate(String date) {
        List<RequestDetail> lstRequestDetail = repos.findByUseDate(date);
        if(!lstRequestDetail.isEmpty())
        {
            for(int i = 0;i <lstRequestDetail.size();i++)
            {
                Request request = requestRepos.findByRequestDetails(lstRequestDetail.get(i)).get(0);
                if(lstRequestDetail.get(i).isDeleteStatus() == true && !request.getStatus().equals("APPROVED"))
                {
                    lstRequestDetail.remove(lstRequestDetail.get(i));
                }
            }
            return lstRequestDetail;
        }
        return null;
    }
    @Override
    public RequestDetailDTO mapDTO(RequestDetail requestDetail) {
        RequestDetailDTO requestDetailDTO = new RequestDetailDTO();
        requestDetailDTO.setId(requestDetail.getId());
        requestDetailDTO.setUseDate(requestDetail.getUseDate());
        requestDetailDTO.setTimeUsing(requestDetail.getTimeUsing());
        requestDetailDTO.setDeleteStatus(requestDetail.isDeleteStatus());
        requestDetailDTO.setFacility(requestDetail.getFacility());
        requestDetailDTO.setRequest(requestDetail.getRequest());
        requestDetailDTO.setRequestDetailStatus(requestDetail.getRequestDetailStatus());
        return requestDetailDTO;
    }

    @Override
    public RequestDetail mapEntity(RequestDetailDTO requestDetailDTO) {
        RequestDetail requestDetail = new RequestDetail();
        requestDetail.setId(requestDetailDTO.getId());
        requestDetail.setUseDate(requestDetailDTO.getUseDate());
        requestDetail.setTimeUsing(requestDetailDTO.getTimeUsing());
        requestDetail.setDeleteStatus(requestDetailDTO.isDeleteStatus());
        requestDetail.setFacility(requestDetailDTO.getFacility());
        requestDetail.setRequest(requestDetailDTO.getRequest());
        requestDetail.setRequestDetailStatus(requestDetailDTO.getRequestDetailStatus());
        return requestDetail;
    }

    @Override
    public void deleteRequestDetailDB(RequestDetail requestDetail) {
        repos.delete(requestDetail);
    }

    public void checkDateRequest() throws IOException {
        List<RequestDetail> requestsDetailExpired = repos.findAllByUseDateBeforeAndStatus(new Date(), "APPROVED");
        List<Account> accounts = requestsDetailExpired
                .stream()
                .map(r -> r.getRequest().getAccount())
                .distinct()
                .collect(Collectors.toList());
        File file = ResourceUtils.getFile("src/main/java/fptu/FacilitesOnlineBooking/RejectedEmail.txt");
        String content = new String(Files.readAllBytes(file.toPath()));
        for (Account account : accounts) {
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setFrom("sonbeo710@gmail.com");
            mail.setTo(account.getEmail());
            mail.setSubject("subject");
            mail.setText("text");
            sender.send(mail);
        }
    }
}
