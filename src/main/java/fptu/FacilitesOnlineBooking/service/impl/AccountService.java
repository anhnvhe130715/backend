package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.dto.AccountDTO;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.repository.AccountRepository;
import fptu.FacilitesOnlineBooking.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountService implements IAccountService {
    @Autowired
    AccountRepository repos;
    @Override
    public List<Account> getListAccount() {
        List<Account> result = repos.findAll();
        return result;
    }

    @Override
    public Long addAccount(Account account) {
        repos.save(account);
        return account.getId();
    }

    @Override
    public Account mapEntity(AccountDTO accountDTO) {
        Account account = new Account();
        account.setId(accountDTO.getId());
        account.setEmail(accountDTO.getEmail());
        account.setFullName(accountDTO.getFullName());
        account.setRole(accountDTO.getRole());
        account.setRequests(accountDTO.getRequests());
        account.setRequestLogs(accountDTO.getRequestLogs());
        return account;
    }

    @Override
    public AccountDTO mapDTO(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setEmail(account.getEmail());
        accountDTO.setFullName(account.getFullName());
        accountDTO.setRole(account.getRole());
        accountDTO.setRequests(account.getRequests());
        accountDTO.setRequestLogs(account.getRequestLogs());
        accountDTO.setRequestTypeStatus(account.isRequestTypeStatus());
        return accountDTO;
    }

    @Override
    public List<Account> getListAccountByClub(ClubDetail clubDetail) {
        List<Account> result = repos.findByClubMembers(clubDetail);
        if(result.isEmpty())
        {
            return result;
        }
        else{
            return new ArrayList<>();
        }
    }

    @Override
    public Long updateAccount(Account member) {
        Optional<Account> account = repos.findById(member.getId());
        account.get().setClubMembers(member.getClubMembers());
        account.get().setEventMembers(member.getEventMembers());
        repos.save(account.get());
        return account.get().getId();
    }

    @Override
    public Account getAccountById(Long id) {
        Account acc = repos.findById(id).get();
        return acc;
    }

    @Override
    public List<Account> searchByEmail(String email) {
        return repos.searchByEmail(email);
    }

    @Override
    public Long updateAccountStatus(Account account) {
        repos.save(account);
        return account.getId();
    }

    @Override
    public void deleteAccount(Account account) {
        repos.delete(account);
    }

    @Override
    public List<Account> getListStaffAccount() {
        List<Account> accountList = repos.findAll();
        List<Account> result = new ArrayList<>();
        for(Account acc : accountList)
        {
            if(!acc.getRole().equalsIgnoreCase("ROLE_STUDENT"))
            {
                result.add(acc);
            }
        }
        return result;
    }

    @Override
    public Account getClubEventStatus(String email) {
        Account acc = repos.findByEmail(email);
        return acc;
    }
}
