package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.common.ExcelHelper;
import fptu.FacilitesOnlineBooking.dto.EventDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.repository.ClubRepository;
import fptu.FacilitesOnlineBooking.repository.EventRepository;
import fptu.FacilitesOnlineBooking.service.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class EventService implements IEventService {
    @Autowired
    EventRepository ev;
    @Autowired
    ExcelHelper excel;
    @Override
    public int createEvent(EventDetail eventDetail) {
        ev.save(eventDetail);
        return 1;
    }

    @Override
    public Optional<EventDetail> getEventDetail(Long id) {
        Optional<EventDetail> eventDetail = ev.findById(id);
        if (eventDetail.isPresent() && eventDetail.get().isDeleteStatus() == false) {
            return eventDetail;
        }
        return null;
    }

    @Override
    public List<EventDetail> getAllEvent() {
        List<EventDetail> eventDetails = ev.findAll();
        List<EventDetail> result = new ArrayList<>();
        for (int i = 0; i < eventDetails.size(); i++) {
            if (eventDetails.get(i).isDeleteStatus() == false) {
                result.add(eventDetails.get(i));
            }
        }
        return result;
    }

    @Override
    public List<EventDetail> getEventByName(String eventName) {
        List<EventDetail> eventDetails = ev.findByEventName(eventName);
        List<EventDetail> result = new ArrayList<>();
        if (!eventDetails.isEmpty()) {
            for (int i = 0; i < eventDetails.size(); i++) {
                result.add(eventDetails.get(i));
            }
            return result;
        }
        return null;
    }

    @Override
    public EventDetail updateEvent(EventDetail eventDetail) {
        if (eventDetail.getEventName() != null && eventDetail.isDeleteStatus() == false) {
            EventDetail updatedEvent = ev.save(eventDetail);
            return new EventDetail(updatedEvent.getId(), updatedEvent.getEventName(), updatedEvent.getFromDate(), updatedEvent.getToDate(), updatedEvent.isDeleteStatus(), updatedEvent.getAccounts());
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEvent(long id) {
        try {
            Optional<EventDetail> eventDetail = ev.findById(id);
            if (eventDetail.isPresent() && eventDetail.get().isDeleteStatus() == false) {
                eventDetail.get().setDeleteStatus(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void checkDateEvent() {
        List<EventDetail> eventDetailExpired = ev.findAllByToDateBefore(new Date());
        eventDetailExpired = eventDetailExpired.stream()
                .peek(e -> e.setDeleteStatus(true))
                .collect(Collectors.toList());
        ev.saveAll(eventDetailExpired);
    }

    @Override
    public EventDTO mapDTO(EventDetail eventDetail) {
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId(eventDetail.getId());
        eventDTO.setEventName(eventDetail.getEventName());
        eventDTO.setFromDate(eventDetail.getFromDate());
        eventDTO.setToDate(eventDetail.getToDate());
        eventDTO.setDeleteStatus(eventDetail.isDeleteStatus());
        eventDTO.setAccounts(List.copyOf(eventDetail.getAccounts()));
        return eventDTO;
    }

    @Override
    public EventDetail mapEntity(EventDTO eventDTO) {
        EventDetail eventDetail = new EventDetail();
        eventDetail.setId(eventDTO.getId());
        eventDetail.setEventName(eventDTO.getEventName());
        eventDetail.setFromDate(eventDTO.getFromDate());
        eventDetail.setToDate(eventDTO.getToDate());
        eventDetail.setDeleteStatus(eventDTO.isDeleteStatus());
        eventDetail.setAccounts(Set.copyOf(eventDTO.getAccounts()));
        return eventDetail;
    }

    @Override
    public List<EventDetail> getEventByDeleteStatus() {
        List<EventDetail> result;
        result = ev.findAllByDeleteStatus(true);
        return result;
    }

    @Override
    public FileImportError save(MultipartFile file) {
        FileImportError error = new FileImportError();
        try {
            int countFile = 0;
            int countDB  = 0;
            List<String> errorLine = new ArrayList<>();
            List<EventDetail> eventDetails = excel.csvToEvent(file.getInputStream());
            for(int i = 0; i<eventDetails.size();i++)
            {
                for(int j = i+1;j<eventDetails.size();j++)
                {
                    if(eventDetails.get(i).getEventName().equalsIgnoreCase(eventDetails.get(j).getEventName()))
                    {
                        errorLine.add(eventDetails.get(i).getEventName()+" //in File");
                        countFile++;
                        //throw new RuntimeException("fail to store csv data: Duplicate data in file" + clubDetails.get(i).getClubName());
                    }
                }
                if(!ev.findByEventName(eventDetails.get(i).getEventName()).isEmpty()) {
                    errorLine.add(eventDetails.get(i).getEventName()+ " //in Database");
                    countDB++;
                }
            }
            if(countDB != 0 || countFile != 0)
            {
                error.setErrorLocation(errorLine);
                error.setErrorInDBNumber(countDB);
                error.setErrorInFileNumber(countFile);
            }
            else {
                ev.saveAll(eventDetails);
            }
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
        return error;
    }

    @Override
    public List<EventDetail> getAllOldEvent() {
        List<EventDetail> eventDetails = ev.findAll();
        List<EventDetail> result = new ArrayList<>();
        for (int i = 0; i < eventDetails.size(); i++) {
            if (eventDetails.get(i).isDeleteStatus() == true) {
                result.add(eventDetails.get(i));
            }
        }
        return result;
    }

    @Override
    public void deleteEventDB(EventDetail eventDetail) {
        ev.delete(eventDetail);
    }

    @Override
    public List<EventDetail> getAllEventDB() {
        List<EventDetail> result = new ArrayList<>();
        if(ev.findAll() != null){
            result = ev.findAll();
        }
        return result;
    }

    @Override
    public EventDetail getAllEventDetail(Long id) {
        return ev.findById(id).get();
    }
}
