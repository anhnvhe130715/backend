package fptu.FacilitesOnlineBooking.service.impl;


import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.repository.AccountRepository;
import fptu.FacilitesOnlineBooking.repository.RequestDetailRepository;
import fptu.FacilitesOnlineBooking.repository.RequestRepository;
import fptu.FacilitesOnlineBooking.service.IRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class RequestService implements IRequestService {
    @Autowired
    RequestRepository repos;
    @Autowired
    AccountRepository accountRepository;
    @Override
    public Long createRequest(Request request){
        Account account = accountRepository.findById(request.getAccount().getId()).get();
        if(request.isBookedByClub() == false || accountRepository.findById(request.getAccount().getId()).get().getClubMembers().isEmpty() &&
                accountRepository.findById(request.getAccount().getId()).get().getEventMembers().isEmpty())
        {
            request.setStatus("APPROVED");
        }
        else{
            request.setStatus("PENDING");
        }
        int size =request.getRequestDetails().size();
        repos.save(request);
        return request.getId();
    }

    @Override
    public Optional<Request> getRequestDetail(Long id) {
        Optional<Request> request = repos.findById(id);
        if(request.get().isDeleteStatus() == false && !request.equals(null))
        {
            return request;
        }
        return null;
    }

    @Override
    public List<Request> getRequestByAccountId(Long id) {
        List<Request> requests = repos.findByAccountId(id);
        List<Request> result = new ArrayList<>();
        if(!requests.isEmpty())
        {
            for(int i = 0;i <requests.size();i++)
            {
                if(requests.get(i).isDeleteStatus() == false)
                {
                    result.add(requests.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public List<Request> getRequestByStatus(String status) {
        List<Request> requests = repos.findByStatus(status);
        List<Request> result = new ArrayList<>();
        if(!requests.isEmpty())
        {
            for(int i = 0;i <requests.size();i++)
            {
                if(requests.get(i).isDeleteStatus() == false)
                {
                    result.add(requests.get(i));
                }
            }
            return result;
        }
        return null;
    }
    @Override
    public Long updateRequestStatus(Request request,String status) {
        request.setStatus(status);
        repos.save(request);
        return request.getId();
    }

    @Override
    public Long deleteRequest(Request request) {
        request.setDeleteStatus(true);
        repos.save(request);
        return request.getId();
    }
    @Override
    public RequestDTO mapDTO(Request request)
    {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setId(request.getId());
        requestDTO.setDateCreated(request.getDateCreated());
        requestDTO.setDateModified(request.getDateModified());
        requestDTO.setNote(request.getNote());
        requestDTO.setStatus(request.getStatus());
        requestDTO.setTypeRequest(request.getTypeRequest());
        requestDTO.setRequestDetails(request.getRequestDetails());
        requestDTO.setRequestLogs(request.getRequestLogs());
        requestDTO.setAccount(request.getAccount());
        requestDTO.setBookedByClub(request.isBookedByClub());
        return requestDTO;
    }
    @Override
    public Request mapEntity(RequestDTO requestDTO)
    {
        Request request = new Request();
        request.setId(requestDTO.getId());
        request.setDateCreated(requestDTO.getDateCreated());
        request.setDateModified(requestDTO.getDateModified());
        request.setNote(requestDTO.getNote());
        request.setStatus(requestDTO.getStatus());
        request.setTypeRequest(requestDTO.getTypeRequest());
        request.setRequestDetails(requestDTO.getRequestDetails());
        request.setRequestLogs(requestDTO.getRequestLogs());
        request.setAccount(requestDTO.getAccount());
        request.setBookedByClub(requestDTO.isBookedByClub());
        return request;
    }

    @Override
    public List<Request> getRequestByBookByClub(boolean bookByClub) {
        List<Request> requests = repos.findByIsBookedByClub(bookByClub);
        List<Request> result = new ArrayList<>();
        if(!requests.isEmpty())
        {
            for(int i = 0;i <requests.size();i++)
            {
                if(requests.get(i).isDeleteStatus() == false)
                {
                    result.add(requests.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public void deleteRequestDB(Request request) {
        repos.delete(request);
    }
}
