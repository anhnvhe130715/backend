package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.common.ExcelHelper;
import fptu.FacilitesOnlineBooking.dto.FacilityDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.repository.FacilityRepository;
import fptu.FacilitesOnlineBooking.service.IFacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FacilityService implements IFacilityService {
    @Autowired
    FacilityRepository faci;
    @Autowired
    ExcelHelper excel;
    @Override
    public int createFacility(Facility facility) {
        faci.save(facility);
        return 1;
    }

    @Override
    public Optional<Facility> getFacilityDetail(Long id) {
        Optional<Facility> facility = faci.findById(id);
        if (facility.isPresent() && facility.get().isDeleteStatus() == false) {
            return facility;
        }
        return null;
    }

    @Override
    public List<Facility> getAllFacility() {
        List<Facility> facilities = faci.findAll();
        List<Facility> result = new ArrayList<>();
        for (int i = 0; i < facilities.size(); i++) {
            if (facilities.get(i).isDeleteStatus() == false) {
                result.add(facilities.get(i));
            }
        }

        return result;
    }


    @Override
    public List<Facility> getFacilityByName(String facilityName) {
        List<Facility> facilities = faci.findByFacilityName(facilityName);
        List<Facility> result = new ArrayList<>();
        if (!facilities.isEmpty()) {
            for (int i = 0; i < facilities.size(); i++) {
                if (facilities.get(i).isAvailable() == true && facilities.get(i).isDeleteStatus() == false) {
                    result.add(facilities.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public List<Facility> getRoomByBulding(Long id, boolean bookByClub) {
        List<Facility> facilityList = faci.findByBuildings(faci.findById(id).get());
        List<Facility> result = new ArrayList<>();
        for (int i = 0; i < facilityList.size(); i++) {
            if(bookByClub == false) {
                if (facilityList.get(i).isBuilding() == false && facilityList.get(i).isAvailable() == true && facilityList.get(i).isForClubEvent() == false
                        && facilityList.get(i).isDeleteStatus() == false) {
                    result.add(facilityList.get(i));
                }
            }else{
                if (facilityList.get(i).isBuilding() == false && facilityList.get(i).isAvailable() == true && facilityList.get(i).isDeleteStatus() == false)
                {
                    result.add(facilityList.get(i));
                }
            }
        }
        return result;
    }

    @Override
    public List<Facility> getFacilityByBuilding() {
        List<Facility> facilities = faci.findByisBuilding(true);
        List<Facility> result = new ArrayList<>();
        if (!facilities.isEmpty()) {
            for (int i = 0; i < facilities.size(); i++) {
                if (facilities.get(i).isAvailable() == true && facilities.get(i).isDeleteStatus() == false) {
                    result.add(facilities.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public Facility updateFacility(Facility facility) {
        if (facility.getFacilityName() != null) {
            Facility updatedFacility = faci.save(facility);
            if(facility.getBuildings() != null)
            {
                List<Facility> room = new ArrayList<>();
                room.add(facility);
                facility.getBuildings().setRoom(room);
            }
            return new Facility(updatedFacility.getId(), updatedFacility.getFacilityName(), updatedFacility.isBuilding(), updatedFacility.isBuilding(), updatedFacility.isDeleteStatus(), updatedFacility.getRoom());
        }else {
            return null;
        }

    }

    @Override
    public Long deleteFacility(long id) {
        try {
            Optional<Facility> facility = faci.findById(id);
            if (facility.isPresent() && facility.get().isDeleteStatus() == false) {
                facility.get().setDeleteStatus(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FacilityDTO mapDTO(Facility facility) {
        FacilityDTO facilityDTO = new FacilityDTO();
        facilityDTO.setId(facility.getId());
        facilityDTO.setFacilityName(facility.getFacilityName());
        facilityDTO.setBuilding(facility.isBuilding());
        facilityDTO.setAvailable(facility.isAvailable());
        facilityDTO.setDeleteStatus(facility.isDeleteStatus());
        facilityDTO.setRoom(facility.getRoom());
        facilityDTO.setBuildings(facility.getBuildings());
        facilityDTO.setCapacity(facility.getCapacity());
        facilityDTO.setForClubEvent(facility.isForClubEvent());
        facilityDTO.setNotForPersonal(facility.isNotForPersonal());
        return facilityDTO;
    }

    @Override
    public Facility mapEntity(FacilityDTO facilityDTO) {
        Facility facility = new Facility();
        facility.setId(facilityDTO.getId());
        facility.setFacilityName(facilityDTO.getFacilityName());
        facility.setBuilding(facilityDTO.isBuilding());
        facility.setAvailable(facilityDTO.isAvailable());
        facility.setDeleteStatus(facilityDTO.isDeleteStatus());
        facility.setRoom(facilityDTO.getRoom());
        facility.setBuildings(facilityDTO.getBuildings());
        facility.setCapacity(facilityDTO.getCapacity());
        facility.setForClubEvent(facilityDTO.isForClubEvent());
        facility.setNotForPersonal(facilityDTO.isNotForPersonal());
        return facility;
    }

    @Override
    public FileImportError save(MultipartFile file) {
        FileImportError error = new FileImportError();
        try {
            List<Facility> facilities = excel.csvToFacility(file.getInputStream());
            int countFile = 0;
            int countDB  = 0;
            List<String> errorLine = new ArrayList<>();
            for(int i = 0; i< facilities.size();i++)
            {
                for(int j = i+1; j< facilities.size();j++)
                {
                    if (facilities.get(i).getFacilityName().equalsIgnoreCase(facilities.get(j).getFacilityName())
                    ) {
                        errorLine.add(facilities.get(i).getFacilityName()+" //in File");
                        countFile++;
                    }
                }
                if(!faci.findByFacilityName(facilities.get(i).getFacilityName()).isEmpty())
                {
                    errorLine.add(facilities.get(i).getFacilityName()+ " //in Database");
                    countDB++;
                }
                if(countDB != 0 || countFile != 0)
                {
                    error.setErrorLocation(errorLine);
                    error.setErrorInDBNumber(countDB);
                    error.setErrorInFileNumber(countFile);
                }
                else {
                    faci.saveAll(facilities);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
        return error;
    }

    @Override
    public List<Facility> getAllFacilityDB() {
        List<Facility> result = faci.findAll();
        return result;
    }

    @Override
    public void deleteFacilityDB(Facility facility) {
        if(facility.isBuilding() == true)
        {
            for(int i = 0; i < facility.getRoom().size();i++)
            {
                faci.delete(facility.getRoom().get(i));
            }
        }
        faci.delete(facility);
    }

    @Override
    public Facility getFacilityDetailDB(Long id) {
        Facility facility = faci.findById(id).get();
        return facility;
    }

}
