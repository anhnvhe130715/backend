package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.repository.AccountRepository;
import fptu.FacilitesOnlineBooking.service.IJwtAccountDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Service("userDetailsService")
public class JwtAccountDetailsService implements IJwtAccountDetailService, UserDetailsService {
    @Autowired
    AccountRepository repos;

    @Override
    public int addAccount(Account account) {
        repos.save(account);
        return 1;
    }

    @Override
    public Account getAccountDetail(String email) {
       return repos.findByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account acc = repos.findByEmail(email);
        if(!(acc == null) && !(acc.getPassWord() == null))
        {
            return new User(acc.getEmail(),acc.getPassWord(),getAuthority(acc));
        }else if(!(acc == null) && (acc.getPassWord() == null))
        {
            return new User(acc.getEmail(),"",getAuthority(acc));
        }
        else {
            throw new UsernameNotFoundException("Email not found with username: " + email);
        }
    }
    private Set getAuthority(Account account) {
        Set authorities = new HashSet<>();
            authorities.add(new SimpleGrantedAuthority(account.getRole()));
        return authorities;
    }
}
