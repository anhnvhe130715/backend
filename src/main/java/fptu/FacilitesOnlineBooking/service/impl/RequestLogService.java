package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.dto.RequestLogDTO;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.model.RequestLog;
import fptu.FacilitesOnlineBooking.repository.RequestLogRepository;
import fptu.FacilitesOnlineBooking.service.IRequestLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
@Transactional
public class RequestLogService implements IRequestLogService {
    @Autowired
    RequestLogRepository repos;
    @Override
    public Long createRequestLog(RequestLog requestLog) {
        repos.save(requestLog);
        return requestLog.getId();
    }

    @Override
    public Long deleteRequestLog(Long id) {
        RequestLog requestLog = repos.getById(id);
        requestLog.setDeleteStatus(true);
        repos.save(requestLog);
        return 0L;
    }

    @Override
    public List<RequestLog> getListRequestLogByAccount(Long id) {
        return null;
    }

    @Override
    public List<RequestLog> getListRequest() {
        List<RequestLog> lstRequestDetail = repos.findAll();
        List<RequestLog> result = new ArrayList<>();
        if (!lstRequestDetail.isEmpty()) {
            for (int i = 0; i < lstRequestDetail.size(); i++) {
                if (lstRequestDetail.get(i).isDeleteStatus() == false) {
                    result.add(lstRequestDetail.get(i));
                }
            }
            return result;
        }
        return  null;
    }

    @Override
    public List<RequestLog> getRequestLogByTypeChange(String typeChange) {
        List<RequestLog> lstRequestDetail = repos.findByTypeChange(typeChange);
        List<RequestLog> result = new ArrayList<>();
        if (!lstRequestDetail.isEmpty()) {
            for (int i = 0; i < lstRequestDetail.size(); i++) {
                result.add(lstRequestDetail.get(i));
            }
            return result;
        }
        return null;
    }

    @Override
    public List<RequestLog> getRequestLogByDateLog(String dateLog) {
        List<RequestLog> lstRequestDetail = repos.findByDateLog(dateLog);
        List<RequestLog> result = new ArrayList<>();
        if (!lstRequestDetail.isEmpty()) {
            for (int i = 0; i < lstRequestDetail.size(); i++) {
                result.add(lstRequestDetail.get(i));
            }
            return result;
        }
        return null;

    }

    @Override
    public RequestLogDTO mapDTO(RequestLog requestLog) {
        RequestLogDTO requestLogDTO = new RequestLogDTO();
        requestLogDTO.setId(requestLog.getId());
        requestLogDTO.setAccount(requestLog.getAccount());
        requestLogDTO.setDateLog(requestLog.getDateLog());
        requestLogDTO.setTypeChange(requestLog.getTypeChange());
        requestLogDTO.setDeleteStatus(requestLog.isDeleteStatus());
        return requestLogDTO;
    }

    @Override
    public RequestLog mapEntity(RequestLogDTO requestLogDTO) {
        RequestLog requestLog = new RequestLog();
        requestLog.setId(requestLogDTO.getId());
        requestLog.setAccount(requestLogDTO.getAccount());
        requestLog.setDateLog(requestLogDTO.getDateLog());
        requestLog.setTypeChange(requestLogDTO.getTypeChange());
        requestLog.setDeleteStatus(requestLogDTO.isDeleteStatus());
        return requestLog;
    }

    @Override
    public void deleteRequestLogDB(RequestLog requestLog) {
        repos.delete(requestLog);
    }

    @Override
    public List<RequestLog> getListRequestLogByRequest(Request request) {
        List<RequestLog> lstRequestLog = repos.findByRequest(request);
        List<RequestLog> result = new ArrayList<>();
        if(!lstRequestLog.isEmpty())
        {
            for(int i = 0;i <lstRequestLog.size();i++)
            {
                if(lstRequestLog.get(i).isDeleteStatus() == false)
                {
                    result.add(lstRequestLog.get(i));
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public RequestLog getRequestLogDetail(Long id) {
        return repos.findById(id).get();
    }
}
