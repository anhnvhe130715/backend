package fptu.FacilitesOnlineBooking.service.impl;

import fptu.FacilitesOnlineBooking.model.Email;
import fptu.FacilitesOnlineBooking.repository.EmailRepository;
import fptu.FacilitesOnlineBooking.service.IEmailService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class EmailService implements IEmailService {
    @Autowired
    EmailRepository repos;
    private final JavaMailSender sender;

    @Override
    public void send(String to, String subject, String content) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom("sonbeo710@gmail.com");
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setText(content);
        sender.send(mail);
    }

    @Override
    public List<Email> getContent(String subject) {
        return repos.findAllBySubject(subject);
    }
}
