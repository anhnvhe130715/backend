package fptu.FacilitesOnlineBooking.service;

import fptu.FacilitesOnlineBooking.dto.AccountDTO;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.ClubDetail;

import java.util.List;

public interface IAccountService {
    List<Account> getListAccount();
    Long addAccount(Account account);
    Account mapEntity (AccountDTO accountDTO);
    AccountDTO mapDTO(Account account);
    List<Account> getListAccountByClub(ClubDetail clubDetail);
    Long updateAccount(Account account);
    Account getAccountById(Long id);
    List<Account> searchByEmail(String email);
    Long updateAccountStatus(Account account);
    void deleteAccount(Account account);
    List<Account> getListStaffAccount();
    Account getClubEventStatus(String email);
}
