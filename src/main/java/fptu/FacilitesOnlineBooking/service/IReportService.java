package fptu.FacilitesOnlineBooking.service;


import fptu.FacilitesOnlineBooking.dto.ReportDTO;

import java.util.List;

public interface IReportService {
    void sendReport(ReportDTO reportDTO);
}
