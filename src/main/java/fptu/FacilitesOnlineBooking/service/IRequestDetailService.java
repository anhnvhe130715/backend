package fptu.FacilitesOnlineBooking.service;

import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.dto.RequestDetailDTO;
import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IRequestDetailService {
    Long createRequest(RequestDetail requestDetail);

    RequestDetail getRequestDetail(Long id);

    Long deleteRequestDetail(RequestDetail requestDetail );

    Optional<RequestDetail> getRequestByRequest(Request request);
    List<RequestDetail> getRequestDetailByFacility(Facility facility );
    List<RequestDetail> getRequestDetailByRequest(Request request );
    Long updateRequestDetail(RequestDetail requestDetail,String status);
    RequestDetailDTO mapDTO(RequestDetail requestDetail);
    List<RequestDetail> getRequestDetailByDate(String date);
    RequestDetail mapEntity(RequestDetailDTO requestDetailDTO);
    void deleteRequestDetailDB(RequestDetail requestDetail);

}
