package fptu.FacilitesOnlineBooking.service;

import fptu.FacilitesOnlineBooking.model.Email;

import java.util.List;

public interface IEmailService {
    void send(String to, String subject,String content);
    List<Email> getContent(String subject);
}
