package fptu.FacilitesOnlineBooking.service;

import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.model.Request;

import java.util.List;
import java.util.Optional;

public interface IRequestService {
    Long createRequest(Request request);

    Optional<Request> getRequestDetail(Long id);

    List<Request> getRequestByAccountId(Long Id);

    List<Request> getRequestByStatus(String status);

    Long updateRequestStatus(Request request,String Status);

    Long deleteRequest(Request request);

    RequestDTO mapDTO(Request request);

    Request mapEntity(RequestDTO requestDTO);
    List<Request> getRequestByBookByClub(boolean bookByClub);
    void deleteRequestDB(Request request);
}
