package fptu.FacilitesOnlineBooking.service;


import fptu.FacilitesOnlineBooking.dto.ClubDTO;
import fptu.FacilitesOnlineBooking.dto.EventDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface IEventService {
    int createEvent(EventDetail eventDetail);

    Optional<EventDetail> getEventDetail(Long id);

    List<EventDetail> getAllEvent();

    List<EventDetail> getEventByName(String clubName);

    EventDetail updateEvent(EventDetail eventDetail);

    Long deleteEvent(long id);

    void checkDateEvent();

    EventDTO mapDTO(EventDetail eventDetail);

    EventDetail mapEntity(EventDTO eventDTO);

    List<EventDetail> getEventByDeleteStatus();

    FileImportError save(MultipartFile file);

    List<EventDetail> getAllOldEvent();

    void deleteEventDB(EventDetail eventDetail);

    List<EventDetail> getAllEventDB();

    EventDetail getAllEventDetail(Long id);
}
