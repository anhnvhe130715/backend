package fptu.FacilitesOnlineBooking.service;


import fptu.FacilitesOnlineBooking.dto.ClubDTO;
import fptu.FacilitesOnlineBooking.dto.FacilityDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.Facility;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface IFacilityService {
    int createFacility(Facility facility);

    Optional<Facility> getFacilityDetail(Long id);

    List<Facility> getAllFacility();

    List<Facility> getFacilityByName(String facilityName);

    List<Facility> getRoomByBulding(Long id, boolean bookByClub);

    List<Facility> getFacilityByBuilding();

    Facility updateFacility(Facility facility);

    Long deleteFacility(long id);

    FacilityDTO mapDTO(Facility facility);

    Facility mapEntity(FacilityDTO facilityDTO);

    FileImportError save(MultipartFile file);

    List<Facility> getAllFacilityDB();

    void deleteFacilityDB(Facility facility);
    Facility getFacilityDetailDB(Long id);
}
