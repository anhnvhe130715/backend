package fptu.FacilitesOnlineBooking.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import fptu.FacilitesOnlineBooking.dto.ClubDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.model.Request;
import org.springframework.web.multipart.MultipartFile;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;

public interface IClubService {
    int createClub(ClubDetail clubDetail);

    Optional<ClubDetail> getClubDetail(Long id);

    List<ClubDetail> getAllClub();

    List<ClubDetail> getClubByName(String clubName);

    ClubDetail updateClub(ClubDetail clubDetail);

    Long deleteClub(long id);

    ClubDTO mapDTO(ClubDetail clubDetail);

    ClubDetail mapEntity(ClubDTO clubDTO);

    FileImportError save(MultipartFile file);

    void deleteClubDB(ClubDetail clubDetail);

    List<ClubDetail> getAllClubDB();
    ClubDetail getClubDetailDB(Long id);
}
