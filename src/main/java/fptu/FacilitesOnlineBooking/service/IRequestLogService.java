package fptu.FacilitesOnlineBooking.service;

import fptu.FacilitesOnlineBooking.dto.RequestLogDTO;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestLog;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IRequestLogService {
    Long createRequestLog(RequestLog requestLog);
    Long deleteRequestLog(Long id);
    List<RequestLog> getListRequestLogByAccount(Long id);
    List<RequestLog> getListRequest();
    List<RequestLog> getRequestLogByTypeChange(String typeChange);
    List<RequestLog> getRequestLogByDateLog(String dateLog);
    RequestLogDTO mapDTO(RequestLog requestLog);
    RequestLog mapEntity(RequestLogDTO requestLogDTO);
    void deleteRequestLogDB(RequestLog requestLog);
    List<RequestLog> getListRequestLogByRequest(Request request);
    RequestLog getRequestLogDetail(Long id);
}
