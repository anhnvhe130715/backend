package fptu.FacilitesOnlineBooking.service;

import fptu.FacilitesOnlineBooking.model.Account;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface IJwtAccountDetailService {
     int addAccount(Account account);
     Account getAccountDetail(String email);
}
