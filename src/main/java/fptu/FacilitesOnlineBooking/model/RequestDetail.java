package fptu.FacilitesOnlineBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="request_detail")
public class RequestDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column (name="use_date")
    private Date useDate;
    @Column(name="time_using")
    private String timeUsing;
    @Column(name="delete_status")
    private boolean deleteStatus;
    @Column(name="semester_booked")
    private boolean isSemesterBooked;
    @ManyToOne
    @JoinColumn(name="facility_id")
    private Facility facility;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    @JsonIgnore
    private Request request;
    @Column(name = "request_detail_status")
    private String requestDetailStatus;
    public RequestDetail(Date useDate) {
        this.useDate = useDate;
    }

}
