package fptu.FacilitesOnlineBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="club_detail")
public class ClubDetail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "club_name")
    private String clubName;
    @Column(name="delete_status")
    private boolean deleteStatus;
    @ManyToMany(mappedBy = "clubMembers", fetch = FetchType.EAGER)
    private Set<Account> accounts;

    public ClubDetail(String clubName, boolean deleteStatus) {
        this.clubName = clubName;
        this.deleteStatus = deleteStatus;
    }
}
