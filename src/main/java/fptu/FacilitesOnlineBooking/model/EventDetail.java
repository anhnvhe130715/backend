package fptu.FacilitesOnlineBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="event_detail")
public class EventDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="event_name")
    private String eventName;
    @Column(name="from_date")
    private Date fromDate;
    @Column(name="to_date")
    private Date toDate;
    @Column(name="delete_status")
    private boolean deleteStatus;
    @ManyToMany(mappedBy = "eventMembers", fetch = FetchType.EAGER)
    private Set<Account> accounts;

    public EventDetail(String eventName, Date fromDate, Date toDate, boolean deleteStatus) {
        this.eventName = eventName;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.deleteStatus = deleteStatus;
    }
}
