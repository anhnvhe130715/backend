package fptu.FacilitesOnlineBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="facility")
public class Facility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="facility_name")
    private String facilityName;
    @Column(name="is_building")
    private boolean isBuilding;
    @Column(name="is_avalable")
    private boolean isAvailable;
    @Column(name="delete_status")
    private boolean deleteStatus;
    @ManyToOne
    @JoinColumn(name = "building_id")
    private Facility buildings;
    @OneToMany(mappedBy = "buildings",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Facility> room;
    @Column(name = "capacity")
    private Long capacity;
    @Column(name = "for_club_event")
    private boolean forClubEvent;
    @Column(name = "not_for_personal")
    private boolean isNotForPersonal;
    @OneToMany(mappedBy = "facility",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<RequestDetail> requestDetails;
    public Facility(Long id, String facilityName, boolean isBuilding, boolean isAvailable, boolean deleteStatus, List<Facility> room) {
        this.id = id;
        this.facilityName = facilityName;
        this.isBuilding = isBuilding;
        this.isAvailable = isAvailable;
        this.deleteStatus = deleteStatus;
        this.room = room;
    }

    public Facility(String facilityName, boolean isBuilding, boolean isAvailable, boolean deleteStatus, Facility buildings, Long capacity, boolean forClubEvent, boolean isNotForPersonal) {
        this.facilityName = facilityName;
        this.isBuilding = isBuilding;
        this.isAvailable = isAvailable;
        this.deleteStatus = deleteStatus;
        this.buildings = buildings;
        this.capacity = capacity;
        this.forClubEvent = forClubEvent;
        this.isNotForPersonal = isNotForPersonal;
    }
}
