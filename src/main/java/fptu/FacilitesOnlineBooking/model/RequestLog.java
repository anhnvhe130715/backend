package fptu.FacilitesOnlineBooking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="request_log")
public class RequestLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private Request request;
    @Column (name="date_log")
    private Date dateLog;
    @Column(name="type_change")
    private  String typeChange;
    @Column(name="delete_status")
    private boolean deleteStatus;

    public RequestLog(Account account, Request request, Date dateLog, String typeChange, boolean deleteStatus) {
        this.account = account;
        this.request = request;
        this.dateLog = dateLog;
        this.typeChange = typeChange;
        this.deleteStatus = deleteStatus;
    }
}
