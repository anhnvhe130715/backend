package fptu.FacilitesOnlineBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="request")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="date_created")
    private Date dateCreated;
    @Column(name="date_modified")
    private Date dateModified;
    @Column(name="type_request")
    private String typeRequest;
    @Column(name="note")
    private String note;
    @Column(name="status")
    private String status;
    @Column(name="delete_status")
    private boolean deleteStatus;
    @ManyToOne()
    @JoinColumn(name = "account_id")
    private Account account;
    @OneToMany(mappedBy = "request",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<RequestLog> requestLogs;
    @OneToMany(mappedBy = "request",cascade = CascadeType.ALL)
    @JsonIgnore
    private  List<RequestDetail> requestDetails;
    @Column(name="isBookedByClub")
    private boolean isBookedByClub;
}
