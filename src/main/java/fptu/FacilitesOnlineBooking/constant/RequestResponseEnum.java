package fptu.FacilitesOnlineBooking.constant;

public enum RequestResponseEnum {
    SUCCESS("Your Request has been successfully!"),
    EMPTY("No Request found."),
    EXISTED("This Request is existed."),
    DELETE("Delete Request successfully!"),
    OVER("you already booked 2 times in this semester!"),
    NO_LONGER("You're not in any club or event anymore. Please re-login!");
    private String message;

    private RequestResponseEnum(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }

}
