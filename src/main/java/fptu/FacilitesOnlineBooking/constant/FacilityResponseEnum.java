package fptu.FacilitesOnlineBooking.constant;

public enum FacilityResponseEnum {
    SUCCESS("Your Facility request has been successfully!"),
    EMPTY("No Facility found"),
    EXISTED("This Facility is existed."),
    DELETE("Delete Facility successfully!");
    private String message;

    private FacilityResponseEnum(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }
}
