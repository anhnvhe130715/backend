package fptu.FacilitesOnlineBooking.constant;

public enum BaseMessage {
    SUCCESS("Your request has been successfully!"),
    EMPTY("Not found."),
    EXISTED("Is existed."),
    DELETE("Delete successfully!");
    private String message;

    private BaseMessage(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }
}
