package fptu.FacilitesOnlineBooking.constant;

public enum ClubResponseEnum {
    SUCCESS("Your Club request has been successfully!"),
    EMPTY("No Club found."),
    EXISTED("This Club is existed."),
    DELETE("Delete Club successfully!");
    private String message;

    private ClubResponseEnum(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }
}
