package fptu.FacilitesOnlineBooking.constant;

public enum EventResponseEnum {
    SUCCESS("Your Event request has been successfully!"),
    EMPTY("No Event found."),
    EXISTED("This Event is existed."),
    DELETE("Delete Event successfully!");
    private String message;

    private EventResponseEnum(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }
}
