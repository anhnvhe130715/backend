package fptu.FacilitesOnlineBooking.common;

import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.repository.FacilityRepository;
import fptu.FacilitesOnlineBooking.service.impl.FacilityService;
import lombok.AllArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
@Service
@AllArgsConstructor
public class ExcelHelper {
    @Autowired
    FacilityRepository repos;
    public static String TYPE = "application/vnd.ms-excel";
    static String[] HEADERs = { "Id", "Title"};
    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }
    public List<ClubDetail> csvToClub(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<ClubDetail> clubDetailList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                ClubDetail clubDetail = new ClubDetail(
                        csvRecord.get("Club Name"),false
/*                        csvRecord.get("Description"),
                        Boolean.parseBoolean(csvRecord.get("Published"))*/
                );

                clubDetailList.add(clubDetail);
            }
            fileReader.close();
            return clubDetailList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
    public  List<EventDetail> csvToEvent(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<EventDetail> eventDetails = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                EventDetail eventDetail = new EventDetail(
                        csvRecord.get("Event Name"),
                        new SimpleDateFormat("yyyy-MM-dd").parse(csvRecord.get("From Date")),
                        new SimpleDateFormat("yyyy-MM-dd").parse(csvRecord.get("To Date")),
                        false
/*                        csvRecord.get("Description"),
                        Boolean.parseBoolean(csvRecord.get("Published"))*/
                );

                eventDetails.add(eventDetail);
            }
            fileReader.close();
            return eventDetails;
        } catch (IOException | ParseException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
    public List<Facility> csvToFacility(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<Facility> facilityList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                Facility facility = new Facility(
                        csvRecord.get("Facility Name"),
                        Boolean.valueOf(csvRecord.get("Is Building")),
                        Boolean.valueOf(csvRecord.get("Is Available")),
                        false,
                        repos.findById(Long.parseLong(csvRecord.get("Building ID"))).get(),
                        Long.parseLong(csvRecord.get("Capacity")),
                        Boolean.valueOf(csvRecord.get("Is For Club Event")),
                        Boolean.valueOf(csvRecord.get("Is Not For Personal"))
/*                        csvRecord.get("Description"),
                        Boolean.parseBoolean(csvRecord.get("Published"))*/
                );

                facilityList.add(facility);
            }
            return facilityList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
