package fptu.FacilitesOnlineBooking.controller;


import fptu.FacilitesOnlineBooking.constant.BaseMessage;
import fptu.FacilitesOnlineBooking.constant.ClubResponseEnum;
import fptu.FacilitesOnlineBooking.constant.RequestResponseEnum;
import fptu.FacilitesOnlineBooking.dto.AccountDTO;
import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.model.RequestLog;
import fptu.FacilitesOnlineBooking.service.IEmailService;
import fptu.FacilitesOnlineBooking.service.IRequestDetailService;
import fptu.FacilitesOnlineBooking.service.IRequestLogService;
import fptu.FacilitesOnlineBooking.service.IRequestService;
import fptu.FacilitesOnlineBooking.service.impl.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/api")
public class RequestController {
    @Autowired
    private IRequestService requestService;
    @Autowired
    private IRequestDetailService requestDetailService;
    @Autowired
    private IRequestLogService requestLogService;
    @Autowired
    private IEmailService emailService;
    @Autowired
    private AccountService accountService;
    @PostMapping("/student/addRequest")
    public ResponseEntity<?> createRequest(@RequestBody RequestDTO requestDTO) {
        Request request = requestService.mapEntity(requestDTO);
        int count =1;
        int size = request.getRequestDetails().size();
        for(int i = 0; i<size;i++)
        {
            request.getRequestDetails().get(i).setRequest(request);
            if(request.getTypeRequest().equals("Semester"))
            {
                request.getRequestDetails().get(i).setSemesterBooked(true);
            }
            else
            {
                request.getRequestDetails().get(i).setSemesterBooked(false);
            }
            request.getRequestDetails().get(i).setRequestDetailStatus("Open");
            if(request.getTypeRequest().equals("Semester"))
            {
                if(accountService.getAccountById(request.getAccount().getId()).getClubMembers().size() != 0) {
                    for (int j = 0; j < 13; j++) {
                        RequestDetail requestDetail = new RequestDetail();
                        requestDetail.setRequest(request.getRequestDetails().get(i).getRequest());
                        requestDetail.setRequestDetailStatus(request.getRequestDetails().get(i).getRequestDetailStatus());
                        requestDetail.setDeleteStatus(false);
                        requestDetail.setSemesterBooked(request.getRequestDetails().get(i).isSemesterBooked());
                        requestDetail.setFacility(request.getRequestDetails().get(i).getFacility());
                        requestDetail.setTimeUsing(request.getRequestDetails().get(i).getTimeUsing());
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date date = formatter.parse(request.getRequestDetails().get(i).getUseDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().plusDays((7 * count)).toString());
                            requestDetail.setUseDate(date);
                            request.getRequestDetails().add(requestDetail);
                            requestDetailService.createRequest(requestDetail);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        count++;
                    }
                }else{
                    return ResponseEntity.status(HttpStatus.OK).body(RequestResponseEnum.NO_LONGER.message());
                }
            }
            requestDetailService.createRequest(request.getRequestDetails().get(i));
        }
        RequestLog requestLog = new RequestLog(request.getAccount(), request, new Date(),"ADD",false);
        List<RequestLog> lstRequestLogs = new ArrayList<>();
        lstRequestLogs.add(requestLog);
        request.setRequestLogs(lstRequestLogs);


        requestLogService.createRequestLog(requestLog);
        requestService.createRequest(request);
        return ResponseEntity.ok().body(RequestResponseEnum.SUCCESS.message());
    }
    @PostMapping("/student/addRequestEvent")
    public ResponseEntity<?> createRequestEvent(@RequestBody RequestDTO requestDTO) {
        Account acc = accountService.getAccountById(requestDTO.getAccount().getId());
        if(accountService.getAccountById(requestDTO.getAccount().getId()).getRequestEventCount() < 2 && (accountService.getAccountById(requestDTO.getAccount().getId()).getClubMembers().size() != 0
                || accountService.getAccountById(requestDTO.getAccount().getId()).getEventMembers().size() != 0 ))
        {
            int count = accountService.getAccountById(requestDTO.getAccount().getId()).getRequestEventCount();
            Request request = requestService.mapEntity(requestDTO);
            RequestLog requestLog = new RequestLog(request.getAccount(), request, new Date(),"ADD",false);
            List<RequestLog> lstRequestLogs = new ArrayList<>();
            lstRequestLogs.add(requestLog);
            request.setRequestLogs(lstRequestLogs);
            requestLogService.createRequestLog(requestLog);
            int size = request.getRequestDetails().size();
            for(int i = 0; i<size;i++)
            {
                request.getRequestDetails().get(i).setRequest(request);
                request.getRequestDetails().get(i).setSemesterBooked(false);
                request.getRequestDetails().get(i).setRequestDetailStatus("Open");
                requestDetailService.createRequest(request.getRequestDetails().get(i));
            }
            requestService.createRequest(request);
            count++;
            acc.setRequestEventCount(count);
            accountService.updateAccountStatus(acc);
            return ResponseEntity.ok().body(RequestResponseEnum.SUCCESS.message());
        }
        if(accountService.getAccountById(requestDTO.getAccount().getId()).getRequestEventCount() >= 2) {
            return ResponseEntity.ok().body(RequestResponseEnum.OVER.message());
        }
        return ResponseEntity.ok().body(RequestResponseEnum.NO_LONGER.message());
    }
    @GetMapping("/getRequestDetail/{id}")
    public ResponseEntity<?> getRequestDetail(@PathVariable Long id)
    {
        RequestDTO result = requestService.mapDTO(requestService.getRequestDetail(id).get());
        result.setRequestDetails(requestDetailService.getRequestDetailByRequest(requestService.getRequestDetail(id).get()));
        if(result == null)
        {
            return ResponseEntity.ok().body(RequestResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/student/getRequestByAccountId/{id}")
    public  ResponseEntity<?> getRequestListByAcccountId(@PathVariable Long id)
    {
        List<Request> request = requestService.getRequestByAccountId(id);
        if(request == null)
        {
            return ResponseEntity.ok().body(RequestResponseEnum.EMPTY.message());
        }
        List<RequestDTO> result = new ArrayList<>();
        for(int i = 0;i <request.size();i++)
        {
            RequestDTO requestDTO = requestService.mapDTO(request.get(i));
            requestDTO.setRequestDetails(requestDetailService.getRequestDetailByRequest(request.get(i)));
            result.add(requestDTO);
        }
        return  ResponseEntity.ok().body(result);
    }
    @GetMapping("/icpdp/getPendingRequest")
    public ResponseEntity<?> getPendingRequest(){
        List<Request> request = requestService.getRequestByStatus("PENDING");
        if(request == null)
        {
            return ResponseEntity.ok().body(RequestResponseEnum.EMPTY.message());
        }
        List<RequestDTO> result = new ArrayList<>();
        for(int i = 0;i <request.size();i++)
        {
            RequestDTO requestDTO = requestService.mapDTO(request.get(i));
            requestDTO.setRequestDetails(requestDetailService.getRequestDetailByRequest(request.get(i)));
            result.add(requestDTO);
        }
        Collections.sort(result);
        return  ResponseEntity.ok().body(result);
    }
    @GetMapping("/icpdp/getRequest/{bookByClub}")
    public ResponseEntity<?> getRequest(@PathVariable boolean bookByClub){
        List<Request> request = requestService.getRequestByBookByClub(bookByClub);
        if(request == null)
        {
            return ResponseEntity.ok().body(RequestResponseEnum.EMPTY.message());
        }
        List<RequestDTO> result = new ArrayList<>();
        for(int i = 0;i <request.size();i++)
        {
            RequestDTO requestDTO = requestService.mapDTO(request.get(i));
            requestDTO.setRequestDetails(requestDetailService.getRequestDetailByRequest(request.get(i)));
            result.add(requestDTO);
        }
        Collections.sort(result,Collections.reverseOrder());
        return  ResponseEntity.ok().body(result);
    }
    @PutMapping("/icpdp/updateRequestStatus/{id}")
    public ResponseEntity<?> updateRequestStatus(@PathVariable Long id, @RequestBody RequestDTO requestDTO) throws IOException {
        Optional<Request> requestDetail = requestService.getRequestDetail(id);
        RequestLog requestLog = new RequestLog(requestDetail.get().getAccount(), requestDetail.get(), new Date(),"UPDATE",false);
        List<RequestLog> lstRequestLogs = new ArrayList<>();
        lstRequestLogs.add(requestLog);
        requestDetail.get().getRequestLogs().addAll(lstRequestLogs);
        requestLogService.createRequestLog(requestLog);
        requestService.updateRequestStatus(requestDetail.get() ,requestDTO.getStatus());
        if(requestDetail.get().getStatus().equals("APPROVED")){
            File file = ResourceUtils.getFile("src/main/java/fptu/FacilitesOnlineBooking/ApprovedEmail.txt");
            String content = new String(Files.readAllBytes(file.toPath()));
            emailService.send(requestDetail.get().getAccount().getEmail(),"BOOKING STATUS APPROVED", content);
        }
        else if(requestDetail.get().getStatus().equals("REJECTED"))
        {
            if(requestDetail.get().getTypeRequest().equalsIgnoreCase("Event"))
            {
                int count = requestDetail.get().getAccount().getRequestEventCount();
                count--;
                Account acc = requestDetail.get().getAccount();
                acc.setRequestEventCount(count);
                accountService.updateAccountStatus(acc);
            }
            File file = ResourceUtils.getFile("src/main/java/fptu/FacilitesOnlineBooking/RejectedEmail.txt");
            String content = new String(Files.readAllBytes(file.toPath()));
            emailService.send(requestDetail.get().getAccount().getEmail(),"BOOKING STATUS REJECTED",content);
        }
        return ResponseEntity.ok().body(RequestResponseEnum.SUCCESS.message());
    }
    @PutMapping("/deleteRequest/{id}")
    public ResponseEntity<?> deleteRequest(@PathVariable Long id)
    {
        Optional<Request> request = requestService.getRequestDetail(id);
        RequestLog requestLog = new RequestLog(request.get().getAccount(), request.get(), new Date(),"DELETE",false);
        List<RequestLog> lstRequestLogs = new ArrayList<>();
        lstRequestLogs.add(requestLog);
        request.get().getRequestLogs().addAll(lstRequestLogs);
        requestLogService.createRequestLog(requestLog);
        requestService.deleteRequest(request.get());
        for (RequestDetail requestDetail : request.get().getRequestDetails()) {
            requestDetailService.deleteRequestDetail(requestDetail);
        }
        return ResponseEntity.ok().body(RequestResponseEnum.DELETE.message());
    }
    @DeleteMapping("/admin/deleteRequestDB/{id}")
    public ResponseEntity<?> deleteRequestDB(@PathVariable Long id)
    {
        Optional<Request> request = requestService.getRequestDetail(id);
        requestService.deleteRequestDB(request.get());
        for (RequestDetail requestDetail : request.get().getRequestDetails()) {
            requestDetailService.deleteRequestDetailDB(requestDetail);
        }
        for(RequestLog requestLog : request.get().getRequestLogs())
        {
            requestLogService.deleteRequestLogDB(requestLog);
        }
        /*
        return ResponseEntity.ok().body(RequestResponseEnum.SUCCESS.message());*/
        return ResponseEntity.ok().body(RequestResponseEnum.DELETE.message());
    }
}
