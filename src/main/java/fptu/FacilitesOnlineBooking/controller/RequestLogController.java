package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.constant.BaseMessage;
import fptu.FacilitesOnlineBooking.dto.RequestLogDTO;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestLog;
import fptu.FacilitesOnlineBooking.service.IRequestDetailService;
import fptu.FacilitesOnlineBooking.service.IRequestLogService;
import fptu.FacilitesOnlineBooking.service.IRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/api")
public class RequestLogController {
    @Autowired
    IRequestLogService requestLogService;
    @Autowired
    IRequestDetailService requestDetailService;
    @Autowired
    IRequestService requestService;
    @GetMapping("/icpdp/getListRequestLog")
    public ResponseEntity<?> getListRequestLog(){
        List<RequestLogDTO> result = new ArrayList<>();
        List<RequestLog> requestLogs = requestLogService.getListRequest();
        if(requestLogs == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        for(RequestLog requestLog : requestLogs)
        {
            RequestLogDTO requestLogDTO = requestLogService.mapDTO(requestLog);
            requestLogDTO.setRequest(requestService.mapDTO(requestLog.getRequest()));
            requestLogDTO.getRequest().setRequestDetails(requestDetailService.getRequestDetailByRequest(requestLog.getRequest()));
            result.add(requestLogDTO);
        }
        System.out.println(requestLogs.size());
        return  ResponseEntity.ok().body(result);
    }

    @GetMapping("/icpdp/getRequestLogByTypeChange/{typeChange}")
    public ResponseEntity<?> getListRequestLogByTypeChange(@PathVariable String typeChange) {
        List<RequestLogDTO> result = new ArrayList<>();
        List<RequestLog> requestLogs = requestLogService.getRequestLogByTypeChange(typeChange);
        if(requestLogs == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        for(RequestLog requestLog : requestLogs)
        {
            RequestLogDTO requestLogDTO = requestLogService.mapDTO(requestLog);
            requestLogDTO.setRequest(requestService.mapDTO(requestLog.getRequest()));
            requestLogDTO.getRequest().setRequestDetails(requestDetailService.getRequestDetailByRequest(requestLog.getRequest()));
            result.add(requestLogDTO);
        }
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/icpdp/getRequestLogByDateLog/{dateLog}")
    public ResponseEntity<?> getListRequestLogByDateLog(@PathVariable String dateLog) {
        List<RequestLogDTO> result = new ArrayList<>();
        List<RequestLog> requestLogs = requestLogService.getRequestLogByDateLog(dateLog);
        if(requestLogs == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        for(RequestLog requestLog : requestLogs)
        {
            RequestLogDTO requestLogDTO = requestLogService.mapDTO(requestLog);
            requestLogDTO.setRequest(requestService.mapDTO(requestLog.getRequest()));
            requestLogDTO.getRequest().setRequestDetails(requestDetailService.getRequestDetailByRequest(requestLog.getRequest()));
            result.add(requestLogDTO);
        }
        return ResponseEntity.ok().body(result);
    }
    @DeleteMapping("/icpdp/deleteRequestLog/{id}")
    public ResponseEntity<?> deleteRequestLogDB(@PathVariable Long id) {
    RequestLog requestLog = requestLogService.getRequestLogDetail(id);
    requestLogService.deleteRequestLogDB(requestLog);
    return ResponseEntity.ok().body("Deleted");
    }
}
