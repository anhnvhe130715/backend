package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.common.ExcelHelper;
import fptu.FacilitesOnlineBooking.constant.ClubResponseEnum;
import fptu.FacilitesOnlineBooking.constant.FacilityResponseEnum;
import fptu.FacilitesOnlineBooking.constant.RequestResponseEnum;
import fptu.FacilitesOnlineBooking.dto.FacilityDTO;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.*;
import fptu.FacilitesOnlineBooking.service.IFacilityService;
import fptu.FacilitesOnlineBooking.service.IRequestDetailService;
import fptu.FacilitesOnlineBooking.service.IRequestLogService;
import fptu.FacilitesOnlineBooking.service.IRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.io.dozer.CsvDozerBeanWriter;
import org.supercsv.io.dozer.ICsvDozerBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/api")
public class FacilityController {
    @Autowired
    private IFacilityService facilityService;
    @Autowired
    private IRequestService requestService;
    @Autowired
    private IRequestDetailService requestDetailService;
    @Autowired
    private IRequestLogService requestLogService;
    @PostMapping("/icpdp/addFacility")
    public ResponseEntity<?> createFacility(@RequestBody FacilityDTO facilityDTO) {
        if (facilityService.getFacilityByName(facilityDTO.getFacilityName()) == null){
            Facility facility = facilityService.mapEntity(facilityDTO);
            facilityService.createFacility(facility);
            return ResponseEntity.ok().body(FacilityResponseEnum.SUCCESS.message());
        }
        return ResponseEntity.ok().body(FacilityResponseEnum.EXISTED.message());
    }

    @GetMapping("/Facility")
    public ResponseEntity<?> findAll() {
        List<Facility> result = facilityService.getAllFacility();
        if (result == null) {
            return ResponseEntity.ok().body(FacilityResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/getFacilityDetail/{id}")
    public ResponseEntity<?> getFacilityDetail(@PathVariable Long id) {
        Optional<Facility> result = facilityService.getFacilityDetail(id);
        if (result == null) {
            return ResponseEntity.ok().body(FacilityResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result.get());
    }


    @GetMapping("/getFacilityByName/{name}")
    public ResponseEntity<?> getFacilityByName(@PathVariable String name) {
        List<Facility> result = facilityService.getFacilityByName(name);
        if (result == null) {
            return ResponseEntity.ok().body(FacilityResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/icpdp/updateFacility/{id}")
    public ResponseEntity<?> updateFacility(@PathVariable Long id, @RequestBody Facility facility) {
        Facility facilityDetail = facilityService.getFacilityDetail(id).get();
        facilityDetail.setFacilityName(facility.getFacilityName());
        facilityDetail.setAvailable(facility.isAvailable());
        facilityDetail.setBuilding(facility.isBuilding());
        facilityDetail.setCapacity(facility.getCapacity());
        facilityDetail.setForClubEvent(facility.isForClubEvent());
        facilityDetail.setNotForPersonal(facility.isNotForPersonal());
        facilityDetail.setBuildings(facility.getBuildings());
        facilityService.updateFacility(facilityDetail);
        return ResponseEntity.ok().body(FacilityResponseEnum.SUCCESS.message());
    }

    @PutMapping("/icpdp/deleteFacility/{id}")
    public ResponseEntity<?> deleteFacility(@PathVariable Long id) {
        Optional<Facility> facility = facilityService.getFacilityDetail(id);
        facilityService.deleteFacility(facility.get().getId());
        return ResponseEntity.ok().body(FacilityResponseEnum.DELETE.message());
    }

    @GetMapping("/getRoomByBulding/{id}/{bookByClub}")
    public ResponseEntity<?> getRoomByBuilding(@PathVariable Long id, @PathVariable boolean bookByClub) {
        List<Facility> result = facilityService.getRoomByBulding(id,bookByClub);
        if(result == null)
        {
            return ResponseEntity.ok().body(FacilityResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/getFacilityByBuilding")
    public ResponseEntity<?> getFacilityByBuilding() {
        List<Facility> result = facilityService.getFacilityByBuilding();
        if(result == null)
        {
            return ResponseEntity.ok().body(RequestResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @ExceptionHandler(MultipartException.class)
    @PostMapping("/icpdp/uploadFacility")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelHelper.hasCSVFormat(file)) {
            try {
                FileImportError error = facilityService.save(file);
                if(error.getErrorInFileNumber() == 0 && error.getErrorInDBNumber() == 0) {
                    message = "Uploaded the file successfully: " + file.getOriginalFilename();
                    return ResponseEntity.status(HttpStatus.OK).body(message);
                }else
                {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
                }
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("ERROR");
            }
        }
        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("CSV");
    }
    @GetMapping("/icpdp/exportFacility")
    public void exportToCSV(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=clubs_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);

        List<Facility> facilities = facilityService.getAllFacility();
        ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"ID", "Facility Name","Is Building","Is Available", "Building ID","Capacity","Is For Club Event","Is Not For Personal"};
        String[] nameMapping = {"id", "facilityName","building","available","buildings.id","capacity","forClubEvent","notForPersonal"};
        csvWriter.configureBeanMapping(Facility.class, nameMapping);
        csvWriter.writeHeader(csvHeader);

        for (Facility facility : facilities) {
            csvWriter.write(facility);
        }

        csvWriter.close();

    }
    @GetMapping("/icpdp/exportFacilityTemplate")
    public void exportToCSVTemplate(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=clubs_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);

        List<Facility> facilities = facilityService.getAllFacility();
        ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"ID", "Facility Name","Is Building","Is Available", "Building ID","Capacity","Is For Club Event","Is Not For Personal"};
        String[] nameMapping = {"id", "facilityName","building","available","buildings.id","capacity","forClubEvent","notForPersonal"};
        csvWriter.configureBeanMapping(Facility.class, nameMapping);
        csvWriter.writeHeader(csvHeader);
        Facility facility = new Facility("Example Name",true,true,false,new Facility(),200L,true,true);
        csvWriter.write(facility);
        csvWriter.close();

    }
    @GetMapping("/icpdp/getAllFacilityDB")
    public ResponseEntity<?> findAllDB() {
        List<Facility> result = facilityService.getAllFacilityDB();
        if (result == null) {
            return ResponseEntity.ok().body(FacilityResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @DeleteMapping("/icpdp/deleteFacilityDB/{id}")
    public ResponseEntity<?> deleteFacilityDB(@PathVariable Long id) {
        Facility facility = facilityService.getFacilityDetailDB(id);
        if(facility.isBuilding() == true)
        {
            return ResponseEntity.ok().body("Can't Delete a building. Please delete Room");
        }
        if(facility.getRequestDetails().size() != 0) {
            for (RequestDetail requestDetail : facility.getRequestDetails()) {
                requestService.deleteRequest(requestDetail.getRequest());
                requestDetailService.deleteRequestDetail(requestDetail);
                requestLogService.getListRequestLogByRequest(requestDetail.getRequest());
            }
        }
        facilityService.deleteFacilityDB(facility);

        return ResponseEntity.ok().body(FacilityResponseEnum.DELETE.message());
    }
}
