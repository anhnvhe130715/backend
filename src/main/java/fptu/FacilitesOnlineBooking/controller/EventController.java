package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.common.ExcelHelper;
import fptu.FacilitesOnlineBooking.constant.BaseMessage;
import fptu.FacilitesOnlineBooking.constant.ClubResponseEnum;
import fptu.FacilitesOnlineBooking.constant.EventResponseEnum;
import fptu.FacilitesOnlineBooking.constant.RequestResponseEnum;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.model.EventDetail;
import fptu.FacilitesOnlineBooking.service.IAccountService;
import fptu.FacilitesOnlineBooking.service.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class EventController {
    @Autowired
    private IEventService eventService;
    @Autowired
    private IAccountService accountService;

    @PostMapping("/icpdp/addEventDetail")
    public ResponseEntity<?> createClub(@RequestBody EventDetail eventDetail) {
        if (eventService.getEventByName(eventDetail.getEventName()) == null) {
            eventService.createEvent(eventDetail);
            for (int i = 0; i < eventDetail.getAccounts().size(); i++) {
                List<EventDetail> lstClubDetails = new ArrayList<>();
                lstClubDetails.add(eventDetail);
                Account acc = accountService.getAccountById(List.copyOf(eventDetail.getAccounts()).get(i).getId());
                acc.getEventMembers().addAll(lstClubDetails);
                accountService.addAccount(acc);
            }
            return ResponseEntity.ok().body(EventResponseEnum.SUCCESS.message());
        }
        return ResponseEntity.ok().body(EventResponseEnum.EXISTED.message());
    }

    @GetMapping("/EventDetail")
    public ResponseEntity<?> findAll() {
        List<EventDetail> result = eventService.getAllEvent();
        if (result == null) {
            return ResponseEntity.ok().body(EventResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/icpdp/OldEventDetail")
    public ResponseEntity<?> getOldEvent() {
        List<EventDetail> result = eventService.getAllOldEvent();
        if(result == null)
        {
            return ResponseEntity.ok().body(EventResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/getEventDetail/{id}")
    public ResponseEntity<?> getFacilityDetail(@PathVariable Long id) {
        Optional<EventDetail> result = eventService.getEventDetail(id);
        if (result == null) {
            return ResponseEntity.ok().body(EventResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result.get());
    }


    @GetMapping("/icpdp/getEventByName/{eventName}")
    public ResponseEntity<?> getFacilityByName(@PathVariable String eventName) {
        List<EventDetail> result = eventService.getEventByName(eventName);
        if (result == null) {
            return ResponseEntity.ok().body(EventResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/icpdp/updateEvent/{id}")
    public ResponseEntity<?> updateEvent(@PathVariable Long id, @RequestBody EventDetail eventDetail) {
        EventDetail eventdetails = eventService.getEventDetail(id).get();
        eventdetails.setEventName(eventDetail.getEventName());
        eventdetails.setFromDate(eventDetail.getFromDate());
        eventdetails.setToDate(eventDetail.getToDate());
        if (eventdetails.getAccounts().size() == 0) {
            eventdetails.setAccounts(eventDetail.getAccounts());
            eventService.updateEvent(eventdetails);
        } else {
            eventdetails.getAccounts().addAll(eventDetail.getAccounts());
            eventService.updateEvent(eventdetails);
        }
        if (eventDetail.getAccounts().size() != 0) {
            for (int i = 0; i < eventDetail.getAccounts().size(); i++) {
                List<EventDetail> lstEventDetails = new ArrayList<>();
                lstEventDetails.add(eventdetails);
                Account acc = accountService.getAccountById(List.copyOf(eventDetail.getAccounts()).get(i).getId());
                acc.getEventMembers().addAll(lstEventDetails);
                accountService.updateAccount(acc);
            }
        }
        return ResponseEntity.ok().body(EventResponseEnum.SUCCESS.message());
    }

    @PutMapping("/icpdp/deleteEvent/{id}")
    public ResponseEntity<?> deleteEvent(@PathVariable Long id) {
        Optional<EventDetail> eventDetail = eventService.getEventDetail(id);
        for(Account account : eventDetail.get().getAccounts())
        {
            account.getEventMembers().remove(eventDetail.get());
        }
        eventService.deleteEvent(eventDetail.get().getId());
        return ResponseEntity.ok().body(EventResponseEnum.DELETE.message());
    }

    @GetMapping("/icpdp/findByDeleteStatus")
    public ResponseEntity<?> findByDeleteStatus() {
        List<EventDetail> result = eventService.getEventByDeleteStatus();
        if(result == null)
        {
            return ResponseEntity.ok().body(EventResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/icpdp/deleteEventMember/{accId}/{eventId}")
    public ResponseEntity<?> deleteEventMember(@PathVariable Long accId, @PathVariable Long eventId) {
        for (Account acc : eventService.getEventDetail(eventId).get().getAccounts()) {
            if (acc.getId() == accId) {
                acc.getEventMembers().remove(eventService.getEventDetail(eventId).get());
            }
        }
        eventService.getEventDetail(eventId).get().getAccounts().remove(accountService.getAccountById(accId));
        return ResponseEntity.ok().body(EventResponseEnum.DELETE.message());
    }
    @PostMapping("/icpdp/uploadEvent")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelHelper.hasCSVFormat(file)) {
            try {
                FileImportError error = eventService.save(file);
                if(error.getErrorInFileNumber() == 0 && error.getErrorInDBNumber() == 0) {
                    message = "Uploaded the file successfully: " + file.getOriginalFilename();
                    return ResponseEntity.status(HttpStatus.OK).body(message);
                }else
                {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
                }
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("ERROR");
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("CSV");
    }
    @GetMapping("/icpdp/exportEvent")
    public void exportToCSV(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=clubs_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);

        List<EventDetail> eventDetails = eventService.getAllEvent();

        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"ID", "Event Name","From Date","To Date"};
        String[] nameMapping = {"id", "eventName","fromDate","toDate"};

        csvWriter.writeHeader(csvHeader);

        for (EventDetail eventDetail : eventDetails) {
            csvWriter.write(eventDetail, nameMapping);
        }

        csvWriter.close();

    }
    @GetMapping("/icpdp/exportEventTemplate")
    public void exportToCSVTemplate(HttpServletResponse response) throws IOException, ParseException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=clubs_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"ID", "Event Name","From Date","To Date"};
        String[] nameMapping = {"id", "eventName","fromDate","toDate"};
        csvWriter.writeHeader(csvHeader);
        EventDetail eventDetail = new EventDetail("Example Name",new SimpleDateFormat("yyyy-MM-dd").parse("2021-11-11"),new SimpleDateFormat("yyyy-MM-dd").parse("2021-11-11"),false);
        csvWriter.write(eventDetail, nameMapping);
        csvWriter.close();
    }
    @DeleteMapping("/icpdp/deleteEvent/{id}")
    public ResponseEntity<?> deleteEventDB(@PathVariable Long id) {
        EventDetail eventDetail = eventService.getAllEventDetail(id);
        for (Account acc : eventDetail.getAccounts()) {
            acc.getEventMembers().remove(eventDetail);
            System.out.println("nice");
        }
        eventService.deleteEventDB(eventDetail);
        return ResponseEntity.ok().body(ClubResponseEnum.DELETE.message());
    }
    @GetMapping("/icpdp/getAllEventDB")
    public ResponseEntity<?> getAllEventDB() {
        List<EventDetail> result = eventService.getAllEventDB();
        if(result == null)
        {
            return ResponseEntity.ok().body(EventResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
}
