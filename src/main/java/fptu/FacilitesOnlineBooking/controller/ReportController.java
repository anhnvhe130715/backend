package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.dto.ReportDTO;
import fptu.FacilitesOnlineBooking.service.IReportService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class ReportController {
    private final IReportService reportService;
    @PostMapping("/report")
    public ResponseEntity<String> sendReport(@RequestBody ReportDTO reportDTO) {
        reportService.sendReport(reportDTO);
        return ResponseEntity.ok().body("test");
    }
}
