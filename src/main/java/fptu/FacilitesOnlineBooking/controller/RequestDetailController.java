package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.constant.BaseMessage;
import fptu.FacilitesOnlineBooking.constant.RequestResponseEnum;
import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.dto.RequestDetailDTO;
import fptu.FacilitesOnlineBooking.model.Facility;
import fptu.FacilitesOnlineBooking.model.Request;
import fptu.FacilitesOnlineBooking.model.RequestDetail;
import fptu.FacilitesOnlineBooking.service.IFacilityService;
import fptu.FacilitesOnlineBooking.service.IRequestDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/api")
public class RequestDetailController {
    @Autowired
    private IRequestDetailService requestDetailService;
    @Autowired
    private IFacilityService facilityService;
    @GetMapping("/getRequestDetailByFacility/{id}")
    public ResponseEntity<List<List<RequestDetailDTO>>> getRequestDetail(@PathVariable Long id) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Facility facility = facilityService.getFacilityDetail(id).get();
        LocalDate currentDate = LocalDate.now();
        currentDate = currentDate.minusDays(currentDate.getDayOfWeek().getValue() - 1);
        List<RequestDetailDTO> requestDetailList = new ArrayList<>();
        if(requestDetailService.getRequestDetailByFacility(facility) != null) {
            for (RequestDetail rd : requestDetailService.getRequestDetailByFacility(facility)
            ) {
                requestDetailList.add(requestDetailService.mapDTO(rd));
            }
        }
        List<RequestDetailDTO> listDate = new ArrayList<>();
        List<List<RequestDetailDTO>> result = new ArrayList<>();
        for(int i =0;i<28;i++)
        {
            System.out.println(currentDate.plusDays(i));
            listDate.add(new RequestDetailDTO(formatter.parse(currentDate.plusDays(i).toString())));//from(currentDate.plusDays(i).atStartOfDay(ZoneId.systemDefault()).toInstant())));
            if((i + 1) % 7 == 0)
            {
                for(int j = 0; j < 7; j++)
                {
                    if(requestDetailList != null) {
                        for (int k = 0; k < requestDetailList.size(); k++) {
                            if (formatter.format(listDate.get(j).getUseDate()).equals(formatter.format(requestDetailList.get(k).getUseDate()))) {
                                if (listDate.get(j).getId() == null) {
                                    listDate.set(j, requestDetailList.get(k));
                                } else
                                    listDate.add(requestDetailList.get(k));
                            }
                        }
                    }
                }
                Collections.sort(listDate);
                result.add(listDate);
                listDate = new ArrayList<>();
            }
        }
        for(int i = 0; i < result.size();i++)
        {
            for(int j = 0; j < result.get(i).size();j++)
            {
                System.out.println(result.get(i).get(j).getUseDate().toString());
            }
        }
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/getRequestDetailByDate/{date}")
    public ResponseEntity<?> getRequestDetailByDate(@PathVariable String date) throws ParseException {
        List<RequestDetailDTO> requestDetailList = new ArrayList<>();
        List<RequestDetail> lstDate = requestDetailService.getRequestDetailByDate(date);
        if(lstDate == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        for (RequestDetail rd:  lstDate
        ) {
            requestDetailList.add(requestDetailService.mapDTO(rd));
        }
        return ResponseEntity.ok().body(requestDetailList);
    }
    @PutMapping("/updateRequestDetailStatus/{id}")
    public ResponseEntity<?> updateRequestStatus(@PathVariable Long id, @RequestBody RequestDetailDTO requestDetailDTO){
        RequestDetail requestDetail = requestDetailService.getRequestDetail(id);
        requestDetailService.updateRequestDetail(requestDetail ,requestDetailDTO.getRequestDetailStatus());
        return ResponseEntity.ok().body(RequestResponseEnum.SUCCESS.message());
    }
}
