package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.dto.JwtRequest;
import fptu.FacilitesOnlineBooking.dto.JwtResponse;
import fptu.FacilitesOnlineBooking.configuration.JwtTokenUtil;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.service.IJwtAccountDetailService;
import fptu.FacilitesOnlineBooking.service.impl.JwtAccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticate")
public class LoginController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    JwtAccountDetailsService jwtAccountDetailService;
    @Autowired
    private IJwtAccountDetailService accDetailsService;
    @Autowired
    PasswordEncoder passwordEncode;
    @PostMapping(value = "/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        if(!authenticationRequest.getEmail().contains("@fpt.edu.vn"))
        {
            return ResponseEntity.badRequest().body("Only login with FPT University email");
        }
        Account accountDto = accDetailsService.getAccountDetail(authenticationRequest.getEmail());
        Account addAccount =  new Account();
        if(accountDto == null)
        {
            addAccount.setEmail(authenticationRequest.getEmail());
            addAccount.setFullName(authenticationRequest.getFullName());
            addAccount.setRole("ROLE_STUDENT");
            accDetailsService.addAccount(addAccount);
        }
        UserDetails account = jwtAccountDetailService.loadUserByUsername(authenticationRequest.getEmail());
        String token;
        if(!authenticationRequest.getPassWord().isEmpty())
        {
            try {
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), authenticationRequest.getPassWord()));
            } catch (DisabledException e) {
                throw new Exception("USER_DISABLED", e);
            } catch (BadCredentialsException e) {
                throw new Exception("INVALID_CREDENTIALS", e);
            }
            token = jwtTokenUtil.generateToken(account);
        }
        else{
            token = jwtTokenUtil.generateToken(authenticationRequest.getEmail());
        }
         accountDto = accDetailsService.getAccountDetail(authenticationRequest.getEmail());
        if(accountDto.getClubMembers() == null && accountDto.getEventMembers() == null)
        {
            return ResponseEntity.ok(new JwtResponse(token, accountDto.getEmail(), accountDto.getFullName(), accountDto.getRole(), accountDto.getId(), accountDto.isRequestTypeStatus(),false,false));
        }
        else {
            if (accountDto.getClubMembers().size() != 0 && accountDto.getEventMembers().size() != 0) {
                return ResponseEntity.ok(new JwtResponse(token, accountDto.getEmail(), accountDto.getFullName(), accountDto.getRole(), accountDto.getId(), accountDto.isRequestTypeStatus(), true,true));
            } else if(accountDto.getClubMembers().size() == 0 && accountDto.getEventMembers().size() != 0){
                return ResponseEntity.ok(new JwtResponse(token, accountDto.getEmail(), accountDto.getFullName(), accountDto.getRole(), accountDto.getId(), accountDto.isRequestTypeStatus(), false,true));
            }
            else if(accountDto.getClubMembers().size() != 0 && accountDto.getEventMembers().size() == 0)
            {
                return ResponseEntity.ok(new JwtResponse(token, accountDto.getEmail(), accountDto.getFullName(), accountDto.getRole(), accountDto.getId(), accountDto.isRequestTypeStatus(), true,false));
            }
            else{
                return ResponseEntity.ok(new JwtResponse(token, accountDto.getEmail(), accountDto.getFullName(), accountDto.getRole(), accountDto.getId(), accountDto.isRequestTypeStatus(), false,false));
            }
        }
    }
}
