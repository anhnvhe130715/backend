package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.service.IEmailService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class EmailController {
    private final IEmailService emailService;
    @GetMapping("/email")
    public ResponseEntity<String> SendEmail() {
        emailService.send("sonnghe130316@fpt.edu.vn", "CONFIRM REQUEST","test");
        return ResponseEntity.ok().body("test");
    }
}
