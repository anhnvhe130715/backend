package fptu.FacilitesOnlineBooking.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import fptu.FacilitesOnlineBooking.common.ExcelHelper;
import fptu.FacilitesOnlineBooking.constant.ClubResponseEnum;
import fptu.FacilitesOnlineBooking.dto.FileImportError;
import fptu.FacilitesOnlineBooking.model.Account;
import fptu.FacilitesOnlineBooking.model.ClubDetail;
import fptu.FacilitesOnlineBooking.service.IAccountService;
import fptu.FacilitesOnlineBooking.service.IClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ClubController {
    @Autowired
    private IClubService clubService;
    @Autowired
    private IAccountService accountService;

    @PostMapping("/icpdp/addClubDetail")
    public ResponseEntity<?> createClub(@RequestBody ClubDetail clubDetail) {
        if (clubService.getClubByName(clubDetail.getClubName()) == null) {
            clubService.createClub(clubDetail);
            for (int i = 0; i < clubDetail.getAccounts().size(); i++) {
                List<ClubDetail> lstClubDetails = new ArrayList<>();
                lstClubDetails.add(clubDetail);
                Account acc = accountService.getAccountById(List.copyOf(clubDetail.getAccounts()).get(i).getId());
                acc.getClubMembers().addAll(lstClubDetails);
                accountService.updateAccount(acc);
            }
            return ResponseEntity.ok().body(ClubResponseEnum.SUCCESS.message());
        }
        return ResponseEntity.ok().body(ClubResponseEnum.EXISTED.message());
    }

    @GetMapping("/ClubDetail")
    public ResponseEntity<?> findAll() {
        List<ClubDetail> result = clubService.getAllClub();
        if (result == null) {
            return ResponseEntity.ok().body(ClubResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/getClubDetail/{id}")
    public ResponseEntity<?> getFacilityDetail(@PathVariable Long id) {
        Optional<ClubDetail> result = clubService.getClubDetail(id);
        if (result == null) {
            return ResponseEntity.ok().body(ClubResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result.get());
    }


    @GetMapping("/icpdp/getClubByName/{clubName}")
    public ResponseEntity<?> getFacilityByName(@PathVariable String clubName) {
        List<ClubDetail> result = clubService.getClubByName(clubName);
        if (result == null) {
            return ResponseEntity.ok().body(ClubResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/icpdp/updateClub/{id}")
    public ResponseEntity<?> updateClub(@PathVariable Long id, @RequestBody ClubDetail clubDetail) {
        ClubDetail cluBdetail = clubService.getClubDetail(id).get();
        cluBdetail.setClubName(clubDetail.getClubName());
        if (cluBdetail.getAccounts().size() == 0) {
            cluBdetail.setAccounts(clubDetail.getAccounts());
            clubService.updateClub(cluBdetail);
        } else {
            cluBdetail.getAccounts().addAll(clubDetail.getAccounts());
            clubService.updateClub(cluBdetail);
        }
        if (clubDetail.getAccounts().size() != 0) {
            for (int i = 0; i < clubDetail.getAccounts().size(); i++) {
                List<ClubDetail> lstClubDetails = new ArrayList<>();
                lstClubDetails.add(cluBdetail);
                Account acc = accountService.getAccountById(List.copyOf(clubDetail.getAccounts()).get(i).getId());
                acc.getClubMembers().addAll(lstClubDetails);
                accountService.updateAccount(acc);
            }
        }
        return ResponseEntity.ok().body(ClubResponseEnum.SUCCESS.message());
    }

    @PutMapping("/icpdp/deleteClub/{id}")
    public ResponseEntity<?> deleteClub(@PathVariable Long id) {
        Optional<ClubDetail> clubDetail = clubService.getClubDetail(id);
        for(Account account : clubDetail.get().getAccounts())
        {
            account.getClubMembers().remove(clubDetail.get());
        }
        clubService.deleteClub(clubDetail.get().getId());
        return ResponseEntity.ok().body(ClubResponseEnum.DELETE.message());
    }

    @DeleteMapping("/icpdp/deleteClubMember/{accId}/{clubId}")
    public ResponseEntity<?> deleteClubMember(@PathVariable Long accId, @PathVariable Long clubId) {
        for (Account acc : clubService.getClubDetail(clubId).get().getAccounts()) {
            if (acc.getId() == accId) {
                acc.getClubMembers().remove(clubService.getClubDetail(clubId).get());
                System.out.println("nice");
            }
        }
        clubService.getClubDetail(clubId).get().getAccounts().remove(accountService.getAccountById(accId));
        return ResponseEntity.ok().body(ClubResponseEnum.DELETE.message());
    }

    @GetMapping("/icpdp/exportClub")
    public void exportToCSV(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=clubs_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);

        List<ClubDetail> clubDetails = clubService.getAllClub();

        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"ID", "Club Name"};
        String[] nameMapping = {"id", "clubName"};

        csvWriter.writeHeader(csvHeader);

        for (ClubDetail clubDetail : clubDetails) {
            csvWriter.write(clubDetail, nameMapping);
        }

        csvWriter.close();

    }
    @GetMapping("/icpdp/exportClubTemplate")
    public void exportToCSVTemplate(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=clubs_" + currentDateTime + ".csv";
        response.setHeader(headerKey, headerValue);
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"ID", "Club Name"};
        String[] nameMapping = {"id", "clubName"};
        csvWriter.writeHeader(csvHeader);
        ClubDetail clubDetail = new ClubDetail("Example Name",false);
        csvWriter.write(clubDetail, nameMapping);
        csvWriter.close();

    }
    @PostMapping("/icpdp/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelHelper.hasCSVFormat(file)) {
            try {
                FileImportError error = clubService.save(file);
                if(error.getErrorInFileNumber() == 0 && error.getErrorInDBNumber() == 0) {
                    message = "Uploaded the file successfully: " + file.getOriginalFilename();
                    return ResponseEntity.status(HttpStatus.OK).body(message);
                }
                else
                {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
                }
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("ERROR");
            }
        }
        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("CSV file Only");
    }
    @DeleteMapping("/icpdp/deleteClub/{id}")
    public ResponseEntity<?> deleteClubDB(@PathVariable Long id) {
        ClubDetail clubDetail = clubService.getClubDetailDB(id);
        for (Account acc : clubDetail.getAccounts()) {
                acc.getClubMembers().remove(clubDetail);
                System.out.println("nice");
        }
        clubService.deleteClubDB(clubDetail);
        return ResponseEntity.ok().body(ClubResponseEnum.DELETE.message());
    }
    @GetMapping("/icpdp/getAllClubDB")
    public ResponseEntity<?> findAllDB() {
        List<ClubDetail> result = clubService.getAllClubDB();
        if (result == null) {
            return ResponseEntity.ok().body(ClubResponseEnum.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
}
