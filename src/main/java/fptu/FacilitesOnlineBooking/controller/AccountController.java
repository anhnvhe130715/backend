package fptu.FacilitesOnlineBooking.controller;

import fptu.FacilitesOnlineBooking.constant.BaseMessage;
import fptu.FacilitesOnlineBooking.constant.RequestResponseEnum;
import fptu.FacilitesOnlineBooking.dto.AccountDTO;
import fptu.FacilitesOnlineBooking.dto.RequestDTO;
import fptu.FacilitesOnlineBooking.model.*;
import fptu.FacilitesOnlineBooking.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AccountController {
    @Autowired
    IAccountService accountService;
    @Autowired
    private IRequestService requestService;
    @Autowired
    private IRequestDetailService requestDetailService;
    @Autowired
    private IRequestLogService requestLogService;
    @GetMapping("/icpdp/ListAllAccount")
    public ResponseEntity<?> findAll() {
        List<Account> result = accountService.getListAccount();
        if(result == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @PostMapping("/admin/addAccount")
        public ResponseEntity<?> addAccount (@RequestBody AccountDTO accountDTO)
    {
        Long result;
        if(!accountDTO.getEmail().contains("@fpt.edu.vn"))
        {
            return ResponseEntity.ok().body("this is not a FPT email!");
        }
        if(!accountService.searchByEmail(accountDTO.getEmail()).isEmpty() && !accountDTO.getRole().equalsIgnoreCase("ROLE_STUDENT"))
        {
            accountService.searchByEmail(accountDTO.getEmail()).get(0).setRole(accountDTO.getRole());
             result = accountService.searchByEmail(accountDTO.getEmail()).get(0).getId();
        }else {
         result = accountService.addAccount(accountService.mapEntity(accountDTO));
    }
        return ResponseEntity.ok().body(result);
    }
    @PutMapping("/icpdp/SetRequestTypeStatus/{status}")
    public  ResponseEntity<?> setRequestTypeStatus(@PathVariable boolean status){
        List<Account> lstAccounts = accountService.getListAccount();
        for (Account acc: lstAccounts
             ) {
            acc.setRequestTypeStatus(status);
            accountService.updateAccountStatus(acc);
        }
        return ResponseEntity.ok().body("Successfully");
    }
    @PutMapping("/icpdp/SetEventCount")
    public  ResponseEntity<?> setRequestEventCount(){
        List<Account> lstAccounts = accountService.getListAccount();
        for (Account acc: lstAccounts
        ) {
            acc.setRequestEventCount(0);
            accountService.updateAccountStatus(acc);
        }
        return ResponseEntity.ok().body("Successfully");
    }
    @PutMapping("/icpdp/updateAccount/{id}")
    public  ResponseEntity<?> setRequestTypeStatus(@PathVariable Long id,@RequestBody AccountDTO accountDTO){
        Account account = accountService.getAccountById(id);
            account.setFullName(accountDTO.getFullName());
            account.setRole(accountDTO.getRole());
            accountService.updateAccountStatus(account);
        return ResponseEntity.ok().body("Successfully");
    }
    @GetMapping("/icpdp/ListAllAccount/{email}")
    public ResponseEntity<?> searchByEmail(@PathVariable String email) {
        List<Account> result = accountService.searchByEmail(email);
        if(result == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/icpdp/accountDetail/{id}")
    public ResponseEntity<?> getAccountDetail(@PathVariable Long id) {
        Account result = accountService.getAccountById(id);
        return ResponseEntity.ok().body(result);
    }
    @DeleteMapping("/admin/deleteAccount/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id)
    {
        Account account = accountService.getAccountById(id);
        accountService.deleteAccount(account);
        if(account.getClubMembers().size() != 0)
        {
            for(ClubDetail clubDetail : account.getClubMembers())
            {
                clubDetail.getAccounts().remove(account);
            }
        }
        if(account.getEventMembers().size() != 0)
        {
            for(EventDetail eventDetail : account.getEventMembers())
            {
                eventDetail.getAccounts().remove(account);
            }
        }
        if(account.getRequests().size() != 0) {
            for (Request request : account.getRequests()) {
                requestService.deleteRequestDB(request);
            }
        }
        if(account.getRequestDetails().size() != 0) {
        for (RequestDetail requestDetail : account.getRequestDetails()) {
            requestDetailService.deleteRequestDetailDB(requestDetail);
        }
        }
        if(account.getRequestLogs().size() != 0) {
            for (RequestLog requestLog : account.getRequestLogs()) {
                requestLogService.deleteRequestLogDB(requestLog);
            }
        }
        return ResponseEntity.ok().body(RequestResponseEnum.SUCCESS.message());
    }
    @GetMapping("/icpdp/ListAllStaffAccount")
    public ResponseEntity<?> findAllStaff() {
        List<Account> result = accountService.getListStaffAccount();
        if(result == null)
        {
            return ResponseEntity.ok().body(BaseMessage.EMPTY.message());
        }
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/getEventClubRequest/{email}")
    public ResponseEntity<AccountDTO> getClubEventStatus(@PathVariable String email) {
        Account account = accountService.getClubEventStatus(email);
        AccountDTO result = accountService.mapDTO(account);
        if(account.getEventMembers().size() != 0)
        {
            result.setEventStatus(true);
        }
        if(account.getClubMembers().size() != 0)
        {
            result.setClubStatus(true);
        }
        return ResponseEntity.ok().body(result);
    }
}
