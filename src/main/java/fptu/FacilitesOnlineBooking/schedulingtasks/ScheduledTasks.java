package fptu.FacilitesOnlineBooking.schedulingtasks;

import fptu.FacilitesOnlineBooking.service.impl.EventService;
import fptu.FacilitesOnlineBooking.service.impl.RequestDetailService;
import fptu.FacilitesOnlineBooking.service.impl.RequestService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@AllArgsConstructor
public class ScheduledTasks {
    private final EventService eventService;

    @Scheduled(cron = "0 0 0 * * *") // every day 0:00:00 //
    public void checkDateEvent() { eventService.checkDateEvent(); }

    private final RequestDetailService requestDetailService;

    @Scheduled(cron = "0 0 0 * * *") // every day 0:00:00 //
    public void checkDateRequest() throws IOException { requestDetailService.checkDateRequest(); }
}
